<?php
session_start();
require_once "login_session_validator.php";
require_once "lib/nusoap.php";

//$client = new nusoap_client("http://localhost:8888/reward/userprofile_server.php",false);
include "connection/serverPath.php";
$client = new nusoap_client($serverPath . "userprofile_server.php",false);

if($_POST['getRewardHistory']==true)
{
	$studentID = $_POST['userID'];
	echo $client->call("getRewardHistory",array("studentID"=>$studentID));
	exit();
}

if($_POST['editProfile']==true)
{
	echo $userID = $_POST['userID'];
	echo $year = $_POST['year'];
	echo $track = $_POST['track'];
	echo $email = $_POST['email'];
	echo $tel = $_POST['tel'];
	echo $client->call("editProfile",array("userID"=>$userID, "year"=>$year, "track"=>$track, "email"=>$email, "tel"=>$tel));
	//header("location: profile.php");
	exit();
}

$username = $_POST['username'];
$password = $_POST['password'];

if(!empty($username) && !empty($password))
{
	$userid = $client->call("login",array("username"=>$username,"password"=>$password));
	if($userid == -1) //Authentication Failed
	{
		$_SESSION['authentication'] = false;
		$_SESSION['errorMsg'] = "fail";
		header("location: login.php");
		exit();
	}
	else //Authentication Success
	{
		$_SESSION['authentication'] = true;
		$_SESSION['permissionLevel'] = $client->call("authorize",array("userid"=>$userid));
		$_SESSION['userid'] = $userid;
		if($_SESSION['permissionLevel'] == 'S')
		{
			header("location: profile.php");
			exit();
		}
		else if($_SESSION['permissionLevel'] == 'I')
		{
			header("location: instructor.php");
			exit();
		}
		else if($_SESSION['permissionLevel'] == 'A')
		{
			header("location: admin.php");
			exit();
		}
	}
}
else //Empty field
{
	$_SESSION['errorMsg'] = "empty";
	header("location: login.php");
	exit();
}
?>
<?php
require_once "lib/nusoap.php";

function createConnection(){
	// Create connection
	include "connection/databaseConnection.php";
	$con=mysqli_connect($host,$username,$password,$db_name);
	//$con=mysqli_connect("localhost","root","root","RewardDatabase");
	// Check connection
	if (mysqli_connect_errno()){
		return "Failed to connect to MySQL: " . mysqli_connect_error();
	}
	return $con;
}

function getTotalTicket($uid) {
	$con = createConnection();
	
	$sql = "SELECT ticket_topic, ticket_message, ticket_added FROM Tickets ";
	$sql .= "WHERE By_User_ID = '" . $uid ."'";
	
	$query = mysqli_query($con, $sql);
	
	return mysqli_num_rows($query);
}

function getTotalHighTicket($uid) {
	$con = createConnection();
	
	$sql = "SELECT ticket_topic, ticket_message, ticket_added FROM Tickets ";
	$sql .= "WHERE To_User_ID = '" . $uid ."'";
	
	$query = mysqli_query($con, $sql);
	
	return mysqli_num_rows($query);
}

function getHighUserTicket($uid) {
	$con = createConnection();
	
	$sql = "SELECT * FROM Tickets ";
	$sql .= "WHERE To_User_ID = '" . $uid ."' ORDER BY ticket_id DESC";
	
	$query = mysqli_query($con, $sql);
	
	$dt = array();
	
	while ($rs = mysqli_fetch_array($query)) {
		$sql2 = "SELECT User_Name, User_Surname FROM Users ";
		$sql2 .= "WHERE User_ID = " . $rs['By_User_ID'];
		
		$q2 = mysqli_query($con, $sql2);
		
		$rs2 = mysqli_fetch_assoc($q2);
		$from_user = $rs2['User_Name'] . " " . $rs2['User_Surname'];
		
		$dt[] = array(
			"type"				=>	"high",
			"ticket_id"			=>	$rs['ticket_id'],
			//"By_User_ID"		=>	$rs['By_User_ID'],
			//"To_User_ID"		=>	$rs['To_User_ID'],
			"response_user"		=>	$from_user,
			"ticket_topic"		=>	$rs['ticket_topic'],
			//"ticket_message"	=>	$rs['ticket_message'],
			"ticket_added"		=>	$rs['ticket_added'],
			"ticket_status"		=>	$rs['ticket_status']
		);
	}
	$ret = json_encode($dt);
	return $ret;
}

function getTicket($uid) {
	
	$con = createConnection();
	
	$sql = "SELECT * FROM Tickets ";
	$sql .= "WHERE By_User_ID = '" . $uid ."' ORDER BY ticket_id DESC";
	
	$query = mysqli_query($con, $sql);
	
	$dt = array();
	
	while ($rs = mysqli_fetch_array($query)) {
		
		$sql2 = "SELECT User_Name, User_Surname, Restriction FROM Users ";
		$sql2 .= "WHERE User_ID = " . $rs['To_User_ID'];
		
		$q2 = mysqli_query($con, $sql2);
		
		$rs2 = mysqli_fetch_assoc($q2);
		$to_user = "";
		
		if ($rs2['Restriction'] == 'A') {
			$to_user = "Administrator";
		} else {
			$to_user = $rs2['User_Name'] . " " . $rs2['User_Surname'];
		}
		
		$dt[] = array(
			"type"				=>	"normal",
			"ticket_id"			=>	$rs['ticket_id'],
			//"By_User_ID"		=>	$rs['By_User_ID'],
			//"To_User_ID"		=>	$rs['To_User_ID'],
			"response_user"		=>	$to_user,
			"ticket_topic"		=>	$rs['ticket_topic'],
			//"ticket_message"	=>	$rs['ticket_message'],
			"ticket_added"		=>	$rs['ticket_added'],
			"ticket_status"		=>	$rs['ticket_status']
		);
	}
	$ret = json_encode($dt);
	return $ret;
}

function getComments($tid) {
	
	$con = createConnection();
	
	$sql = "SELECT * FROM TicketComment ";
	$sql .= "WHERE ticket_id = '" . $tid ."' ORDER BY ticket_comment_id ASC";
	
	$query = mysqli_query($con, $sql);
	
	$dt = array();
	
	while ($rs = mysqli_fetch_array($query)) {
		$dt[] = array(
			"uid"			=>	$rs['User_ID'],
			"comment_msg"	=>	$rs['ticket_comment_message'],
			"comment_added"	=>	$rs['ticket_comment_added']
			
		);
	}
	
	$rt = json_encode($dt);
	return $rt;
}

function getOneTicket($uid, $tid, $perm) {
	
	$con = createConnection();
	
	$sql = "SELECT * FROM Tickets ";
	
	if ($perm == 'S') {
		$sql .= "WHERE By_User_ID = " . $uid ." AND ticket_id = " . $tid;
	} else if ($perm == 'I' || $perm == 'A') {
		$sql .= "WHERE To_User_ID = " . $uid ." AND ticket_id = " . $tid;
	}
	
	$query = mysqli_query($con, $sql);
	
	$dt = array();
	
	$rs = mysqli_fetch_assoc($query);
	
	
	$dt[] = array(
		"ticket_id"			=>	$rs['ticket_id'],
		"By_User_ID"		=>	$rs['By_User_ID'],
		"To_User_ID"		=>	$rs['To_User_ID'],
		"ticket_topic"		=>	$rs['ticket_topic'],
		"ticket_message"	=>	$rs['ticket_message'],
		"ticket_added"		=>	$rs['ticket_added'],
		"ticket_status"		=>	$rs['ticket_status']
	);
	
	$ret = json_encode($dt);
	return $ret;
}

function getHighUsers() {
	
	header('Content-type: application/json');
	
	$con = createConnection();
	
	$sql = "SELECT User_ID, User_Name, User_Surname, Restriction FROM Users ";
	$sql .= "WHERE Restriction = 'A' OR Restriction = 'I'";
	
	$query = mysqli_query($con, $sql);
	
	$dt = array();
	
	while ($rs = mysqli_fetch_array($query)) {
		if ($rs['Restriction'] == 'A') {
			$dt[] = array(
				"user_id"			=>	$rs['User_ID'],
				"display_name"		=>	"Administrator"
			);
		} else {
			$dt[] = array(
				"user_id"			=>	$rs['User_ID'],
				"display_name"		=>	"Instructor: " . $rs['User_Name'] . " " . $rs['User_Surname']
			);
		}
	}
	$ret = json_encode($dt);
	return $ret;
}

function getUserFullName($uid) {
	
	$con = createConnection();
	
	$sql = "SELECT User_Name, User_Surname, Restriction FROM Users ";
	$sql .= "WHERE User_ID = " . $uid;
	
	$query = mysqli_query($con, $sql);
	
	$rs = mysqli_fetch_assoc($query);
	
	$rt = "";
	
	if ($rs['Restriction'] == 'A') {
		$rt = "Administrator";
	} else {
		$rt = $rs['User_Name'] . " " . $rs['User_Surname'];
	}
	
	return $rt;
}

function addNewTicket($from_id, $to_id, $topic, $msg) {
	
	$con = createConnection();
	
	$sql = "INSERT INTO Tickets (ticket_id, By_User_ID, To_User_ID, ticket_topic, ticket_message, ticket_added, ticket_status) ";
	$sql .= "VALUES (NULL, '".$from_id."', '".$to_id."', '".$topic."', '".$msg."', NOW(), 'open')";
	
	mysqli_query($con, $sql);
	
	return "200";
	
}

function addNewComment($tid, $uid, $msg) {
	
	$con = createConnection();
	
	$sql = "INSERT INTO TicketComment (ticket_comment_id, ticket_id, User_ID, ticket_comment_message, ticket_comment_added) ";
	$sql .= "VALUES (NULL, ".$tid.", ".$uid.", '".$msg."', NOW())";
	
	mysqli_query($con, $sql);
	
	if (mysqli_error()) {
		return mysqli_error();
	}
	
	return "200";
}

function closeTicket($tid) {
			
	$con = createConnection();
	
	$sql = "UPDATE Tickets SET ticket_status = 'close' WHERE ticket_id = " . $tid;
	
	mysqli_query($con, $sql);
	
	if (mysqli_error()) {
		return mysqli_error();
	}
	
	return "200";
}

function openTicket($tid) {
			
	$con = createConnection();
	
	$sql = "UPDATE Tickets SET ticket_status = 'open' WHERE ticket_id = " . $tid;
	
	mysqli_query($con, $sql);
	
	if (mysqli_error()) {
		return mysqli_error();
	}
	
	return "200";
}

$server = new soap_server();
$server->register("getTotalTicket");
$server->register("getTotalHighTicket");
$server->register("getTicket");
$server->register("getHighUserTicket");
$server->register("getOneTicket");
$server->register("getComments");
$server->register("getHighUsers");
$server->register("getUserFullName");
$server->register("addNewTicket");
$server->register("addNewComment");
$server->register("closeTicket");
$server->register("openTicket");
/*$server->register("login");
$server->register("authorize");
$server->register("getUserInfo");
$server->register("getStudentScore");
$server->register("getBannScore");
$server->register("getRewardHistory");*/

if(!isset($HTTP_RAW_POST_DATA))
	$HTTP_RAW_POST_DATA = file_get_contents('php://input');

$server->service($HTTP_RAW_POST_DATA);

?>
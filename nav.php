<?php
$active = " class=\"active\"";
$home = null;
$profile = null;
$rules = null;
$leaderboard = null;
$contact = null;

if (isset($page)) {
	$val = $page;
	if ($val == "home") {
		$home = $active;
	} else if ($val == "profile") {
		$home = $active;
	} else if ($val == "rules") {
		$rules = $active;
	} else if ($val == "leaderboard") {
		$leaderboard = $active;
	} else if ($val == "contact") {
		$contact = $active;
	} else {
		$home = $active;
	}
} else {
	$home = $active;
}
?>
<div id="head" class="visible-lg visible-md">
    <div class="container">
     <a href="index.php"><img src="img/logo-shadow.png" id="logo-desktop" width="300" height="254" border="0" /></a>
     <ul class="nav nav-pills" id="rw-nav">
      	<li<?=$home?>><a href="index.php">Home</a></li>
     	<li<?=$rules?>><a href="rules.php">Rules</a></li>
    	<li<?=$leaderboard?> class="dropdown">
    		<a href="leaderboard.php" class="dropdown-toggle" data-toggle="dropdown">Leaderboard <b class="caret"></b></a>
    		<ul class="dropdown-menu">
    			<li><a href="leaderboard.php">Student Ranking</a></li>
    			<li><a href="leaderboard.php?rank=baan">Baan Ranking</a></li>
    			<li><a href="leaderboard.php?rank=monthly">Monthly Ranking</a></li>
    		</ul>
    	</li>
    	<li<?=$contact?>><a href="contactus.php">Contact Us</a></li>
  </ul>
</div>
</div>

<div id="head-mb" class="hidden-lg hidden-md">
  <div class="container">
   <div id="main_nav">
    <center><img src="img/logo-shadow.png" width="300" height="254" /></center>
  </div>
</div>
<nav class="navbar navbar-default" role="navigation">
 <div class="container">

  <div class="navbar-header">
   <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
    <span class="sr-only">Toggle navigation</span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
  </button>
</div>

<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
 <ul class="nav navbar-nav">
  <li<?=$home?>><a href="index.php">Home</a></li>
  <li<?=$rules?>><a href="rules.php">Rules</a></li>
  <li<?=$leaderboard?> class="dropdown">
    <a href="leaderboard.php" class="dropdown-toggle" data-toggle="dropdown">Leaderboard <b class="caret"></b></a>
    <ul class="dropdown-menu">
		<li><a href="leaderboard.php">Student Ranking</a></li>
    	<li><a href="leaderboard.php?rank=baan">Baan Ranking</a></li>
    	<li><a href="leaderboard.php?rank=monthly">Monthly Ranking</a></li>
  	</ul>
  </li>
  <li<?=$contact?>><a href="contactus.php">Contact Us</a></li>
</ul>
</div>

</div>
</nav>
</div>
========== Installation ===========

Required: Apache Web Server 2.2.24 or newer
1. Place all of the files in a new folder in htdocs or www folder of your web server
For example: Using MAMP the directory for this folder would be
MAMP/htdocs
So, create a new folder, for example, ‘reward’ and put all the files in MAMP/htdocs/reward
2. Go to ‘connection’ folder
3. Fix the “serverPath.php” to be your path to the servers or the host of your website
For example: If we place all the files in MAMP/htdocs/reward
Browsing “http://localhost/reward/” would means that folder.
*** The host could be “http://localhost:8888/reward/” where 8888 is a specified port ***
So, it is the server path. 
Next, you need to edit $serverPath to contain “http://localhost/reward/”
REMEMBER to end with “/” at the end of the string.
4. Fix the “databaseConnection.php” to connect to your database
$host is your host name.
$username is your username to the database.
$password is your password to the database.
$db_name is the name of the database.

========== Note ==========

'psd' folder and 'less' folder is not a component of a design.

========== Avatar image upload configurations ==========

In 'img' folder, it must has directory 'avatars' inside. Otherwise it will reponses an error while editing the avatar because there is no folder to contain uploaded image files.

For UNIX system, permissions on folder 'img/avatars' (chmod) must be 777.

For Windows system, the users must have at least write permission. 
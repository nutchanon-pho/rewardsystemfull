<?php
require_once "lib/nusoap.php";
function createConnection(){
	// Create connection
	include "connection/databaseConnection.php";
	$con=mysqli_connect($host,$username,$password,$db_name);
	//$con=mysqli_connect("localhost","root","root","RewardDatabase");
	// Check connection
	if (mysqli_connect_errno()){
		return "Failed to connect to MySQL: " . mysqli_connect_error();
	}
	return $con;
}

function getLeaderboard(){
	$con = createConnection();
	$result = mysqli_query($con,"select s.Student_ID, u.User_Name, u.User_Surname, s.Year, s.Bann_ID, Sum(ru.point) as score
	from Students s, Users u, Rules ru, Rewards re
	where s.Student_ID = u.User_ID and ru.Rule_ID = re.Rule_ID and re.Student_ID = s.Student_ID
	group by s.Student_ID, u.User_ID, s.Year
	order by score desc limit 10");
	
	$ret = "";
	$res = "";
    $rank = 1;
	$c = 0;
	$total_rank = mysqli_num_rows($result);
	while($row = mysqli_fetch_array($result))
	{
		$imgavt = file_get_contents("http://".$_SERVER['HTTP_HOST']."/getavt.php?uid=".$row['Student_ID']."&type=leaderboard");
		/*$ret = $ret."<tr><td><b>".$rank."</b></td>
				<td><b>".$row['Student_ID']."</b></td>
				<td><b>".$row['User_Name']."</b></td>
				<td><b>".$row['Year']."</b></td>
				<td><b>".$row['score']."</b></td><tr/>";*/
		if ($rank == 1) {
			$ret .= "<div class=\"container\">\n";
			$ret .= "	<div class=\"row\">\n";
			$ret .= "		<div class=\"col-md-12 col-lg-11 col-centered\">\n";
			$ret .= "			<div class=\"box\">\n";
			$ret .= "				<div class=\"row\">\n";
			$ret .= "					<div class=\"col-xs-3 col-sm-3 col-md-3 col-lg-2\">\n";
			$ret .= "						" . $imgavt . "\n";
			$ret .= "					</div>\n";
			$ret .= "					<div class=\"col-md-9 col-lg-10\">\n";
			$ret .= "						<h2 style=\"font-family: 'gnuolane'\"><strong>".$row['User_Name']." ".$row['User_Surname']."</strong></h2>\n";
			$ret .= "	 					<h4><span class=\"label label-primary\">".$row['Student_ID']."</span> <span class=\"label label-success\">".$row['Year']." Year</span> <span class=\"label label-info\">Bann ".$row['Bann_ID']."</span></h4>\n";
			$ret .= "					</div>\n";
			$ret .= "				</div>\n";
			$ret .= "				<div class=\"row\">\n";
			$ret .= "					<div class=\"col-lg-6\">\n";
			$ret .= "						<center>\n";
			$ret .= "	 						<h1><small>Score:</small><br /><span style=\"font-size: 2.2em; color: #f4c000; font-weight: bold;\">".$row['score']."</span></h1>\n";
			$ret .= "	 					</center>\n";
			$ret .= "					</div>\n";
			$ret .= "					<div class=\"col-lg-4\">\n";
			$ret .= "						<center><img src=\"img/gold.png\" class=\"img-responsive\" /></center>\n";
			$ret .= "					</div>\n";
			$ret .= "					<div class=\"col-lg-2\"></div>\n";
			$ret .= "				</div>\n";
			$ret .= "			</div>\n";
			$ret .= "		</div>\n";
			$ret .= "		<div class=\"col-lg-1\">&nbsp;</div>\n";
			$ret .= "	</div>\n";
			$ret .= "</div>\n";
		}
		if ($rank == 2 || $rank == 3) {
			if ($rank == 2) {
				$ret .= "<div class=\"container\">\n";
				$ret .= "	<div class=\"row\">\n";
				$ret .= "		<div class=\"col-md-12 col-lg-11 col-centered\">\n";
				$ret .= "			<div class=\"row\">\n";
				$ret .= "				<div class=\"col-md-12 col-lg-6\">\n";
				$ret .= "					<div class=\"box\">\n";
				$ret .= "						<div class=\"row\">\n";
				$ret .= "							<div class=\"col-xs-3 col-sm-3 col-md-3 col-lg-3\">\n";
				$ret .= "								" . $imgavt . "\n";
				$ret .= "							</div>\n";
				$ret .= "							<div class=\"col-lg-9\">\n";
				$ret .= "								<h2 style=\"font-family: 'gnuolane'\"><strong>".$row['User_Name']." ".$row['User_Surname']."</strong></h2>\n";
				$ret .= "	 							<h4><span class=\"label label-primary\">".$row['Student_ID']."</span> <span class=\"label label-success\">".$row['Year']." Year</span> <span class=\"label label-info\">Bann ".$row['Bann_ID']."</span></h4>\n";
				$ret .= "							</div>\n";
				$ret .= "						</div>\n";
				$ret .= "						<br />\n";
				$ret .= "						<div class=\"row\">\n";
				$ret .= "							<div class=\"col-lg-6\">\n";
				$ret .= "								<center>\n";
				$ret .= "			 						<h1><small>Score:</small><br /><span style=\"font-size: 1.5em; color: #abb4ac; font-weight: bold;\">".$row['score']."</span></h1>\n";
				$ret .= "			 					</center>\n";
				$ret .= "							</div>\n";
				$ret .= "							<div class=\"col-lg-4\">\n";
				$ret .= "								<center><img src=\"img/silver.png\" class=\"img-responsive\" /></center>\n";
				$ret .= "							</div>\n";
				$ret .= "							<div class=\"col-lg-2\"></div>\n";
				$ret .= "						</div>\n";
				$ret .= "					</div>\n";
				$ret .= "				</div>\n";
			}
			if ($total_rank == 2) {
				$ret .= "				<div class=\"col-md-12 col-lg-6\">\n";
				$ret .= "				</div>\n";
				$ret .= "			</div>\n";
				$ret .= "		</div>\n";
				$ret .= "		<div class=\"col-lg-1\">&nbsp;</div>\n";
				$ret .= "	</div>\n";
				$ret .= "</div>\n";
			}
			if ($rank == 3) {
				$ret .= "				<div class=\"col-md-12 col-lg-6\">\n";
				$ret .= "					<div class=\"box\">\n";
				$ret .= "						<div class=\"row\">\n";
				$ret .= "							<div class=\"col-xs-3 col-sm-3 col-md-3 col-lg-3\">\n";
				$ret .= "								" . $imgavt . "\n";
				$ret .= "							</div>\n";
				$ret .= "							<div class=\"col-lg-9\">\n";
				$ret .= "								<h2 style=\"font-family: 'gnuolane'\"><strong>".$row['User_Name']." ".$row['User_Surname']."</strong></h2>\n";
				$ret .= "	 							<h4><span class=\"label label-primary\">".$row['Student_ID']."</span> <span class=\"label label-success\">".$row['Year']." Year</span> <span class=\"label label-info\">Bann ".$row['Bann_ID']."</span></h4>\n";
				$ret .= "							</div>\n";
				$ret .= "						</div>\n";
				$ret .= "						<br />\n";
				$ret .= "						<div class=\"row\">\n";
				$ret .= "							<div class=\"col-lg-6\">\n";
				$ret .= "								<center>\n";
				$ret .= "			 						<h1><small>Score:</small><br /><span style=\"font-size: 1.5em; color: #cd796a; font-weight: bold;\">".$row['score']."</span></h1>\n";
				$ret .= "			 					</center>\n";
				$ret .= "							</div>\n";
				$ret .= "							<div class=\"col-lg-4\">\n";
				$ret .= "								<center><img src=\"img/bronze.png\" class=\"img-responsive\" /></center>\n";
				$ret .= "							</div>\n";
				$ret .= "							<div class=\"col-lg-2\"></div>\n";
				$ret .= "						</div>\n";
				$ret .= "					</div>\n";
				$ret .= "				</div>\n";
				$ret .= "			</div>\n";
				$ret .= "		</div>\n";
				$ret .= "		<div class=\"col-lg-1\">&nbsp;</div>\n";
				$ret .= "	</div>\n";
				$ret .= "</div>\n";
			}
		}
		if ($total_rank > 3 && $rank == 3) {
			$ret .= "<div class=\"visible-lg\">\n";
			$ret .= "	<br />\n";
			$ret .= "	<center><img src=\"img/ribbon3.png\" class=\"img-responsive visible-lg\" /></center>\n";
			$ret .= "	<br /><br />\n";
			$ret .= "</div>\n";
			$ret .= "<div class=\"container visible-lg\">\n";
			
			$res .= "<div class=\"container hidden-lg\">\n";
			$res .= "	<div class=\"row\">\n";
			$res .= "		<div class=\"col-md-12 col-lg-11 col-centered\">\n";
			$res .= "			<div class=\"row\">\n";
		}
		if ($rank >= 4) {
			if ($rank > 4) {
				$ret .= "	<br />\n";
			}
			$ret .= "	<div class=\"row\">\n";
			$ret .= "		<div class=\"col-md-12 col-lg-11 col-centered\">\n";
			$ret .= "			<div class=\"box\">\n";
			$ret .= "				<div class=\"row\">\n";
			$ret .= "					<div class=\"col-sm-2 col-md-2 col-lg-2\">\n";
			$ret .= "						" . $imgavt . "\n";
			$ret .= "					</div>\n";
			$ret .= "					<div class=\"col-sm-5 col-md-5 col-lg-5\">\n";
			$ret .= "						<h2 style=\"font-family: 'gnuolane'\"><strong>".$row['User_Name']." ".$row['User_Surname']."</strong></h2>\n";
			$ret .= "		 				<h4><span class=\"label label-primary\">".$row['Student_ID']."</span> <span class=\"label label-success\">".$row['Year']." Year</span> <span class=\"label label-info\">Bann ".$row['Bann_ID']."</span></h4>\n";
			$ret .= "					</div>\n";
			$ret .= "					<div class=\"col-sm-3 col-md-3 col-lg-3\">\n";
			$ret .= "						<h1><small>Score:</small><br /><span style=\"font-size: 1.3em; color: #333; font-weight: bold;\">".$row['score']."</span></h1>\n";
			$ret .= "					</div>\n";
			$ret .= "					<div class=\"col-sm-2 col-md-2 col-lg-2\">\n";
			$ret .= "						<center><img src=\"img/".$rank.".png\" class=\"img-responsive\" /></center>\n";
			$ret .= "					</div>\n";
			$ret .= "				</div>\n";
			$ret .= "			</div>\n";
			$ret .= "		</div>\n";
			$ret .= "	</div>\n";
			
			$res .= "			<div class=\"col-md-12 col-lg-6\">\n";
			$res .= "				<div class=\"box\">\n";
			$res .= "					<div class=\"row\">\n";
			$res .= "						<div class=\"col-xs-3 col-sm-3 col-md-3 col-lg-3\">\n";
			$res .= "							" . $imgavt . "\n";
			$res .= "						</div>\n";
			$res .= "						<div class=\"col-lg-9\">\n";
			$res .= "							<h2 style=\"font-family: 'gnuolane'\"><strong>".$row['User_Name']." ".$row['User_Surname']."</strong></h2>\n";
			$res .= " 							<h4><span class=\"label label-primary\">".$row['Student_ID']."</span> <span class=\"label label-success\">".$row['Year']." Year</span> <span class=\"label label-info\">Bann ".$row['Bann_ID']."</span></h4>\n";
			$res .= "						</div>\n";
			$res .= "					</div>\n";
			$res .= "					<br />\n";
			$res .= "					<div class=\"row\">\n";
			$res .= "						<div class=\"col-lg-6\">\n";
			$res .= "							<center>\n";
			$res .= "		 						<h1><small>Score:</small><br /><span style=\"font-size: 1.5em; color: #333; font-weight: bold;\">".$row['score']."</span></h1>\n";
			$res .= "		 					</center>\n";
			$res .= "						</div>\n";
			$res .= "						<div class=\"col-lg-4\">\n";
			$res .= "							<center><img src=\"img/".$rank.".png\" class=\"img-responsive\" /></center>\n";
			$res .= "						</div>\n";
			$res .= "						<div class=\"col-lg-2\"></div>\n";
			$res .= "					</div>\n";
			$res .= "				</div>\n";
			$res .= "			</div>\n";
			
			$c++;
			
			if ($c == 2) {
				$res .= "			</div>\n";
				$res .= "		</div>\n";
				$res .= "		<div class=\"col-lg-2\">&nbsp;</div>\n";
				$res .= "	</div>\n";
				$res .= "</div>\n";
				$res .= "<div class=\"container hidden-lg\">\n";
				$res .= "	<div class=\"row\">\n";
				$res .= "		<div class=\"col-md-12 col-lg-11 col-centered\">\n";
				$res .= "			<div class=\"row\">\n";
				$c = 0;
			}
			
		}
		$rank++;
	}
	mysqli_close($con);
	$ret .= "</div>\n";
	
	if ($c == 0 || $c == 1) {
		$res .= "			</div>\n";
		$res .= "		</div>\n";
		$res .= "		<div class=\"col-lg-2\">&nbsp;</div>\n";
		$res .= "	</div>\n";
		$res .= "</div>\n";
	}
	
	return $ret  . $res;
}

function getLeaderboard_bann(){
	$con = createConnection();
	$result = mysqli_query($con,"select s.Bann_ID, Sum(ru.point) as score
	from Students s, Users u, Rules ru, Rewards re
	where s.Student_ID = u.User_ID and ru.Rule_ID = re.Rule_ID and re.Student_ID = s.Student_ID 
	group by s.Bann_ID
	order by score desc");
	$result_percentage = mysqli_query($con,"select Bann_ID,(100*sum(Point)/(select sum(Point) from Rewards left join Rules on Rules.Rule_ID = Rewards.Rule_ID)) as Percentage
from Rewards
left join Students on Rewards.Student_ID = Students.Student_ID
left join Rules on Rules.Rule_ID = Rewards.Rule_ID
group by Bann_ID
order by Percentage desc");
	$ret = "";
	
	$pc1 = " progress-bar-success";
	$pc2 = " progress-bar-info";
	$pc3 = " progress-bar-warning";
	$pc4 = " progress-bar-danger";
	
	$pc = $pc1;
	
	while($row = mysqli_fetch_array($result))
	{
		
	$row_percentage = mysqli_fetch_array($result_percentage);
	$ret = $ret."
			<div class='panel panel-default'>
				<!-- Default panel contents -->
				<div class='panel-heading' id='Baan".$row['Bann_ID']."' style='cursor:pointer;'>
					<h4>&nbsp;&nbsp;<span class='glyphicon glyphicon-certificate'> </span> Baan".$row['Bann_ID']."</h4>
					<div class='progress'>
						<div class='progress-bar".$pc."' role='progressbar' aria-valuenow='".$row_percentage['Percentage']."' aria-valuemin='0' aria-valuemax='100' style='width: ".$row_percentage['Percentage']."%;'>
							Total: ".$row['score']."
						</div>
					</div>
				</div>
				<!-- Table -->
				<div class=\"table-responsive\">
				<table class='table' id='Baan".$row['Bann_ID']."Table'>
					<thead>
						<tr>
							<th>Rank</th>
							<th>Student ID</th>
							<th>Student Name</th>
							<th>Year</th>
							<th>Score</th>
						</tr>
					</thead>
					<tbody>";
					
					$result2 = mysqli_query($con,"select s.Student_ID, u.User_Name, s.Year, Sum(ru.point) as score
					from Students s, Users u, Rules ru, Rewards re
					where s.Student_ID = u.User_ID and ru.Rule_ID = re.Rule_ID and re.Student_ID = s.Student_ID and s.Bann_ID =".$row['Bann_ID']."
					group by s.Student_ID, u.User_ID, s.Year
					order by score desc");
					
					$rank = 1;
					while($row2 = mysqli_fetch_array($result2))
					{
						$ret = $ret."<tr><td><b>".$rank."</b></td>
								<td><b>".$row2['Student_ID']."</b></td>
								<td><b>".$row2['User_Name']."</b></td>
								<td><b>".$row2['Year']."</b></td>
								<td><b>".$row2['score']."</b></td><tr/>";
						$rank += 1;
					}
		$ret = $ret."</tbody></table></div></div>";
		
		if ($pc == $pc1) {
			$pc = $pc2;
		} else if ($pc == $pc2) {
			$pc = $pc3;
		} else if ($pc == $pc3) {
			$pc = $pc4;
		} else if ($pc == $pc4) {
			$pc = $pc1;
		} else {
			$pc = $pc1;
		}
	}
	mysqli_close($con);
	return $ret;
}

function getLeaderboard_monthly(){
	$con = createConnection();
	$result = mysqli_query($con,"SELECT S.Student_ID, U.User_Name, U.User_Surname, S.Year, S.Bann_ID, SUM( POINT ) as score
FROM  `Rewards` R
LEFT JOIN Users U ON R.Student_ID = U.User_ID
LEFT JOIN Rules RU ON R.Rule_ID = RU.Rule_ID
LEFT JOIN Students S ON S.Student_ID = U.User_ID
WHERE MONTH(Time_Stamp) = MONTH(NOW()) 
AND YEAR(Time_Stamp) = YEAR(NOW()) 
GROUP BY U.User_ID, U.User_Name
ORDER BY SUM( POINT ) DESC 
LIMIT 0 , 10");
	
	$ret = "";
	$res = "";
    $rank = 1;
	$c = 0;
	$total_rank = mysqli_num_rows($result);
	while($row = mysqli_fetch_array($result))
	{
		$imgavt = file_get_contents("http://".$_SERVER['HTTP_HOST']."/getavt.php?uid=".$row['Student_ID']."&type=leaderboard");
		/*$ret = $ret."<tr><td><b>".$rank."</b></td>
				<td><b>".$row['Student_ID']."</b></td>
				<td><b>".$row['User_Name']."</b></td>
				<td><b>".$row['Year']."</b></td>
				<td><b>".$row['score']."</b></td><tr/>";*/
		if ($rank == 1) {
			$ret .= "<div class=\"container\">\n";
			$ret .= "	<div class=\"row\">\n";
			$ret .= "		<div class=\"col-md-12 col-lg-11 col-centered\">\n";
			$ret .= "			<div class=\"box\">\n";
			$ret .= "				<div class=\"row\">\n";
			$ret .= "					<div class=\"col-xs-3 col-sm-3 col-md-3 col-lg-2\">\n";
			$ret .= "						" . $imgavt . "\n";
			$ret .= "					</div>\n";
			$ret .= "					<div class=\"col-md-9 col-lg-10\">\n";
			$ret .= "						<h2 style=\"font-family: 'gnuolane'\"><strong>".$row['User_Name']." ".$row['User_Surname']."</strong></h2>\n";
			$ret .= "	 					<h4><span class=\"label label-primary\">".$row['Student_ID']."</span> <span class=\"label label-success\">".$row['Year']." Year</span> <span class=\"label label-info\">Bann ".$row['Bann_ID']."</span></h4>\n";
			$ret .= "					</div>\n";
			$ret .= "				</div>\n";
			$ret .= "				<div class=\"row\">\n";
			$ret .= "					<div class=\"col-lg-6\">\n";
			$ret .= "						<center>\n";
			$ret .= "	 						<h1><small>Score:</small><br /><span style=\"font-size: 2.2em; color: #f4c000; font-weight: bold;\">".$row['score']."</span></h1>\n";
			$ret .= "	 					</center>\n";
			$ret .= "					</div>\n";
			$ret .= "					<div class=\"col-lg-4\">\n";
			$ret .= "						<center><img src=\"img/gold.png\" class=\"img-responsive\" /></center>\n";
			$ret .= "					</div>\n";
			$ret .= "					<div class=\"col-lg-2\"></div>\n";
			$ret .= "				</div>\n";
			$ret .= "			</div>\n";
			$ret .= "		</div>\n";
			$ret .= "		<div class=\"col-lg-1\">&nbsp;</div>\n";
			$ret .= "	</div>\n";
			$ret .= "</div>\n";
		}
		if ($rank == 2 || $rank == 3) {
			if ($rank == 2) {
				$ret .= "<div class=\"container\">\n";
				$ret .= "	<div class=\"row\">\n";
				$ret .= "		<div class=\"col-md-12 col-lg-11 col-centered\">\n";
				$ret .= "			<div class=\"row\">\n";
				$ret .= "				<div class=\"col-md-12 col-lg-6\">\n";
				$ret .= "					<div class=\"box\">\n";
				$ret .= "						<div class=\"row\">\n";
				$ret .= "							<div class=\"col-xs-3 col-sm-3 col-md-3 col-lg-3\">\n";
				$ret .= "								" . $imgavt . "\n";
				$ret .= "							</div>\n";
				$ret .= "							<div class=\"col-lg-9\">\n";
				$ret .= "								<h2 style=\"font-family: 'gnuolane'\"><strong>".$row['User_Name']." ".$row['User_Surname']."</strong></h2>\n";
				$ret .= "	 							<h4><span class=\"label label-primary\">".$row['Student_ID']."</span> <span class=\"label label-success\">".$row['Year']." Year</span> <span class=\"label label-info\">Bann ".$row['Bann_ID']."</span></h4>\n";
				$ret .= "							</div>\n";
				$ret .= "						</div>\n";
				$ret .= "						<br />\n";
				$ret .= "						<div class=\"row\">\n";
				$ret .= "							<div class=\"col-lg-6\">\n";
				$ret .= "								<center>\n";
				$ret .= "			 						<h1><small>Score:</small><br /><span style=\"font-size: 1.5em; color: #abb4ac; font-weight: bold;\">".$row['score']."</span></h1>\n";
				$ret .= "			 					</center>\n";
				$ret .= "							</div>\n";
				$ret .= "							<div class=\"col-lg-4\">\n";
				$ret .= "								<center><img src=\"img/silver.png\" class=\"img-responsive\" /></center>\n";
				$ret .= "							</div>\n";
				$ret .= "							<div class=\"col-lg-2\"></div>\n";
				$ret .= "						</div>\n";
				$ret .= "					</div>\n";
				$ret .= "				</div>\n";
			}
			if ($total_rank == 2) {
				$ret .= "				<div class=\"col-md-12 col-lg-6\">\n";
				$ret .= "				</div>\n";
				$ret .= "			</div>\n";
				$ret .= "		</div>\n";
				$ret .= "		<div class=\"col-lg-1\">&nbsp;</div>\n";
				$ret .= "	</div>\n";
				$ret .= "</div>\n";
			}
			if ($rank == 3) {
				$ret .= "				<div class=\"col-md-12 col-lg-6\">\n";
				$ret .= "					<div class=\"box\">\n";
				$ret .= "						<div class=\"row\">\n";
				$ret .= "							<div class=\"col-xs-3 col-sm-3 col-md-3 col-lg-3\">\n";
				$ret .= "								" . $imgavt . "\n";
				$ret .= "							</div>\n";
				$ret .= "							<div class=\"col-lg-9\">\n";
				$ret .= "								<h2 style=\"font-family: 'gnuolane'\"><strong>".$row['User_Name']." ".$row['User_Surname']."</strong></h2>\n";
				$ret .= "	 							<h4><span class=\"label label-primary\">".$row['Student_ID']."</span> <span class=\"label label-success\">".$row['Year']." Year</span> <span class=\"label label-info\">Bann ".$row['Bann_ID']."</span></h4>\n";
				$ret .= "							</div>\n";
				$ret .= "						</div>\n";
				$ret .= "						<br />\n";
				$ret .= "						<div class=\"row\">\n";
				$ret .= "							<div class=\"col-lg-6\">\n";
				$ret .= "								<center>\n";
				$ret .= "			 						<h1><small>Score:</small><br /><span style=\"font-size: 1.5em; color: #cd796a; font-weight: bold;\">".$row['score']."</span></h1>\n";
				$ret .= "			 					</center>\n";
				$ret .= "							</div>\n";
				$ret .= "							<div class=\"col-lg-4\">\n";
				$ret .= "								<center><img src=\"img/bronze.png\" class=\"img-responsive\" /></center>\n";
				$ret .= "							</div>\n";
				$ret .= "							<div class=\"col-lg-2\"></div>\n";
				$ret .= "						</div>\n";
				$ret .= "					</div>\n";
				$ret .= "				</div>\n";
				$ret .= "			</div>\n";
				$ret .= "		</div>\n";
				$ret .= "		<div class=\"col-lg-1\">&nbsp;</div>\n";
				$ret .= "	</div>\n";
				$ret .= "</div>\n";
			}
		}
		if ($total_rank > 3 && $rank == 3) {
			$ret .= "<div class=\"visible-lg\">\n";
			$ret .= "	<br />\n";
			$ret .= "	<center><img src=\"img/ribbon3.png\" class=\"img-responsive visible-lg\" /></center>\n";
			$ret .= "	<br /><br />\n";
			$ret .= "</div>\n";
			$ret .= "<div class=\"container visible-lg\">\n";
			
			$res .= "<div class=\"container hidden-lg\">\n";
			$res .= "	<div class=\"row\">\n";
			$res .= "		<div class=\"col-md-12 col-lg-11 col-centered\">\n";
			$res .= "			<div class=\"row\">\n";
		}
		if ($rank >= 4) {
			if ($rank > 4) {
				$ret .= "	<br />\n";
			}
			$ret .= "	<div class=\"row\">\n";
			$ret .= "		<div class=\"col-md-12 col-lg-11 col-centered\">\n";
			$ret .= "			<div class=\"box\">\n";
			$ret .= "				<div class=\"row\">\n";
			$ret .= "					<div class=\"col-sm-2 col-md-2 col-lg-2\">\n";
			$ret .= "						" . $imgavt . "\n";
			$ret .= "					</div>\n";
			$ret .= "					<div class=\"col-sm-5 col-md-5 col-lg-5\">\n";
			$ret .= "						<h2 style=\"font-family: 'gnuolane'\"><strong>".$row['User_Name']." ".$row['User_Surname']."</strong></h2>\n";
			$ret .= "		 				<h4><span class=\"label label-primary\">".$row['Student_ID']."</span> <span class=\"label label-success\">".$row['Year']." Year</span> <span class=\"label label-info\">Bann ".$row['Bann_ID']."</span></h4>\n";
			$ret .= "					</div>\n";
			$ret .= "					<div class=\"col-sm-3 col-md-3 col-lg-3\">\n";
			$ret .= "						<h1><small>Score:</small><br /><span style=\"font-size: 1.3em; color: #333; font-weight: bold;\">".$row['score']."</span></h1>\n";
			$ret .= "					</div>\n";
			$ret .= "					<div class=\"col-sm-2 col-md-2 col-lg-2\">\n";
			$ret .= "						<center><img src=\"img/".$rank.".png\" class=\"img-responsive\" /></center>\n";
			$ret .= "					</div>\n";
			$ret .= "				</div>\n";
			$ret .= "			</div>\n";
			$ret .= "		</div>\n";
			$ret .= "	</div>\n";
			
			$res .= "			<div class=\"col-md-12 col-lg-6\">\n";
			$res .= "				<div class=\"box\">\n";
			$res .= "					<div class=\"row\">\n";
			$res .= "						<div class=\"col-xs-3 col-sm-3 col-md-3 col-lg-3\">\n";
			$res .= "							" . $imgavt . "\n";
			$res .= "						</div>\n";
			$res .= "						<div class=\"col-lg-9\">\n";
			$res .= "							<h2 style=\"font-family: 'gnuolane'\"><strong>".$row['User_Name']." ".$row['User_Surname']."</strong></h2>\n";
			$res .= " 							<h4><span class=\"label label-primary\">".$row['Student_ID']."</span> <span class=\"label label-success\">".$row['Year']." Year</span> <span class=\"label label-info\">Bann ".$row['Bann_ID']."</span></h4>\n";
			$res .= "						</div>\n";
			$res .= "					</div>\n";
			$res .= "					<br />\n";
			$res .= "					<div class=\"row\">\n";
			$res .= "						<div class=\"col-lg-6\">\n";
			$res .= "							<center>\n";
			$res .= "		 						<h1><small>Score:</small><br /><span style=\"font-size: 1.5em; color: #333; font-weight: bold;\">".$row['score']."</span></h1>\n";
			$res .= "		 					</center>\n";
			$res .= "						</div>\n";
			$res .= "						<div class=\"col-lg-4\">\n";
			$res .= "							<center><img src=\"img/".$rank.".png\" class=\"img-responsive\" /></center>\n";
			$res .= "						</div>\n";
			$res .= "						<div class=\"col-lg-2\"></div>\n";
			$res .= "					</div>\n";
			$res .= "				</div>\n";
			$res .= "			</div>\n";
			
			$c++;
			
			if ($c == 2) {
				$res .= "			</div>\n";
				$res .= "		</div>\n";
				$res .= "		<div class=\"col-lg-2\">&nbsp;</div>\n";
				$res .= "	</div>\n";
				$res .= "</div>\n";
				$res .= "<div class=\"container hidden-lg\">\n";
				$res .= "	<div class=\"row\">\n";
				$res .= "		<div class=\"col-md-12 col-lg-11 col-centered\">\n";
				$res .= "			<div class=\"row\">\n";
				$c = 0;
			}
			
		}
		$rank++;
	}
	mysqli_close($con);
	$ret .= "</div>\n";
	
	if ($c == 0 || $c == 1) {
		$res .= "			</div>\n";
		$res .= "		</div>\n";
		$res .= "		<div class=\"col-lg-2\">&nbsp;</div>\n";
		$res .= "	</div>\n";
		$res .= "</div>\n";
	}
	
	return $ret  . $res;
}



$Server = new soap_server();
$Server->register("getLeaderboard");
$Server->register("getLeaderboard_bann");
$Server->register("getLeaderboard_monthly");

if(!isset($HTTP_RAW_POST_DATA))
	$HTTP_RAW_POST_DATA = file_get_contents("php/input");

$Server->service($HTTP_RAW_POST_DATA);

?>
	<div class="modal fade" id="avatar_upload_modal">
	  <div class="modal-dialog">
	    <div class="modal-content">
	    <form action="avtupload.php" id="avt_upload_form" method="post" enctype="multipart/form-data">
		    <div class="modal-header">
		      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		      <h4 class="modal-title">Edit Avatar</h4>
		    </div>
		    <div class="modal-body">
		    	<div class="alert alert-danger" id="avt_error" style="display: none"><strong>Error!</strong> <span id="avt_err"></span></div>
		    	<p><input type="file" name="avt_file"></p>
		    	<div id="avt_preparing" style="display: none">
		    		<br />
		    		<p><img src="img/loading.gif" width="16" height="16" border="0" /> Preparing to upload</p>
		    	</div>
		    	<div id="avt_progress" style="display: none">
		    		<br />
			    	<p><img src="img/loading.gif" width="16" height="16" border="0" /> Uploading <span id="avt_percent">0</span>%</p>
			    	<div class="progress progress-striped">
			    		<div class="progress-bar progress-bar-info" id="avt_progressbar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">
			    		</div>
			    	</div>
		    	</div>
		    </div>
		    <div class="modal-footer">
		      <button type="submit" id="up_avatar" class="btn btn-success"><span class="glyphicon glyphicon-upload"></span> Upload</button>
		      <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
		    </div>
	    </form>
	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
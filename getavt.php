<?php
session_start();

$avt_dir = "img/avatars";

if (is_dir($avt_dir)) {
	
	$fname = array();
	$fext = array();
	
	$opd = opendir($avt_dir);
	while (($fs = readdir($opd)) !== false) {
		if ($fs != "." && $fs != "..") {
			
			$tf = explode(".", $fs);
			
			$ext = end($tf);
			
			$fname[] = $tf[0];
			$fext[] = $ext;
			
			//echo $fs . "<br />";
		}
	}
	
	$uid = md5($_SESSION['userid']);
	
	if (isset($_GET['uid'])) {
		$uid = md5($_GET['uid']);
	}
	
	if (false !== ($key = array_search($uid, $fname))) {
		if (isset($_GET['type'])) {
			if ($_GET['type'] == "leaderboard") {
				echo "<img src=\"img/avatars/" . $uid . "." . $fext[$key] . "?" . time() . "\" class=\"img-responsive img-circle\" id=\"leaderImg\" />";
			} else {
				echo "<img src=\"img/avatars/" . $uid . "." . $fext[$key] . "?" . time() . "\" class=\"img-responsive\" />";
			}
		} else {
			echo "<img src=\"img/avatars/" . $uid . "." . $fext[$key] . "?" . time() . "\" class=\"img-responsive\" />";
		}
	} else {
		if (isset($_GET['type'])) {
			if ($_GET['type'] == "leaderboard") {
				echo "<img src=\"img/person.jpg\" class=\"img-responsive img-circle\" id=\"leaderImg\" />";
			} else {
				echo "<img src=\"img/person.jpg\" class=\"img-responsive\" />";
			}
		} else {
			echo "<img src=\"img/person.jpg\" class=\"img-responsive\" />";
		}
	}
}
?>
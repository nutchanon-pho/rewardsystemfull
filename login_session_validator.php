<?php
// Track the usage time of each user and check if it expires yet
//session_start();
if (isset($_SESSION['most_recent_activity']) && (time() -   $_SESSION['most_recent_activity'] > 600)) 
{
    //600 seconds = 10 minutes
  session_destroy();   
  session_unset();  
  header("location: login.php");
	exit();
}
  $_SESSION['most_recent_activity'] = time(); // the start of the session.
?>
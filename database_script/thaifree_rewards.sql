-- phpMyAdmin SQL Dump
-- version 3.5.8.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 07, 2014 at 10:58 PM
-- Server version: 5.5.31
-- PHP Version: 5.3.24

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `thaifree_rewards`
--

-- --------------------------------------------------------

--
-- Table structure for table `Bann`
--

CREATE TABLE IF NOT EXISTS `Bann` (
  `Bann_ID` int(11) NOT NULL,
  `Bann_Name` varchar(45) NOT NULL,
  `Adviser_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`Bann_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Bann`
--

INSERT INTO `Bann` (`Bann_ID`, `Bann_Name`, `Adviser_ID`) VALUES
(1, 'ONE', 2),
(2, 'TWO', 1),
(3, 'THREE', 1),
(4, 'FOUR', 1),
(5, 'FIVE', 4),
(7, 'SEVEN', 5),
(8, 'EIGHT', 3),
(9, 'NINE', 1);

-- --------------------------------------------------------

--
-- Table structure for table `Rewards`
--

CREATE TABLE IF NOT EXISTS `Rewards` (
  `Reward_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Student_ID` int(11) NOT NULL,
  `Instructor_ID` int(11) NOT NULL,
  `Rule_ID` int(11) NOT NULL,
  `Time_Stamp` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`Reward_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=100 ;

--
-- Dumping data for table `Rewards`
--

INSERT INTO `Rewards` (`Reward_ID`, `Student_ID`, `Instructor_ID`, `Rule_ID`, `Time_Stamp`) VALUES
(60, 5388200, 1, 145, '2014-05-04 10:08:32'),
(61, 5488115, 1, 146, '2014-05-04 10:08:47'),
(62, 5488115, 1, 145, '2014-05-04 10:09:15'),
(63, 5488003, 1, 145, '2014-05-04 10:09:41'),
(64, 5488003, 1, 148, '2014-05-04 10:10:02'),
(65, 5488003, 1, 146, '2014-05-04 10:10:12'),
(66, 5488115, 1, 151, '2014-05-04 10:13:02'),
(67, 5488003, 1, 150, '2014-05-04 10:13:07'),
(68, 5388200, 1, 145, '2014-05-04 10:57:30'),
(69, 5388200, 1, 145, '2014-05-04 11:12:05'),
(70, 5388200, 1, 145, '2014-05-06 02:27:04'),
(71, 5488115, 1, 148, '2014-05-06 09:20:54'),
(72, 5488213, 1, 150, '2014-05-06 12:38:49'),
(73, 5488213, 1, 151, '2014-05-06 12:38:51'),
(74, 5488213, 1, 151, '2014-05-06 12:56:19'),
(75, 5488213, 1, 151, '2014-05-06 12:56:25'),
(76, 5488213, 1, 150, '2014-05-06 12:56:40'),
(77, 5488126, 1, 148, '2014-05-06 13:01:00'),
(78, 5488126, 1, 151, '2014-05-06 13:01:03'),
(79, 5488126, 1, 150, '2014-05-06 13:01:05'),
(80, 5488115, 1, 148, '2014-05-06 13:04:10'),
(81, 5488068, 1, 150, '2014-05-06 13:13:11'),
(82, 5488022, 1, 151, '2014-05-06 13:28:20'),
(83, 5488022, 1, 151, '2014-05-06 13:29:52'),
(84, 5488066, 1, 151, '2014-05-06 13:49:00'),
(85, 5488066, 1, 150, '2014-05-06 13:49:03'),
(86, 5488066, 1, 148, '2014-05-06 13:49:04'),
(87, 5488066, 1, 151, '2014-05-06 13:49:40'),
(88, 5488079, 1, 151, '2014-05-06 13:52:01'),
(89, 5488079, 1, 148, '2014-05-06 13:52:05'),
(90, 5488128, 1, 152, '2014-05-06 13:56:32'),
(91, 5488128, 1, 151, '2014-05-06 13:56:34'),
(92, 5488289, 1, 148, '2014-05-06 13:59:50'),
(93, 5488999, 121, 152, '2014-05-07 15:49:13'),
(94, 5488999, 121, 151, '2014-05-07 15:49:18'),
(95, 5488289, 121, 150, '2014-05-07 15:49:33'),
(96, 5588145, 121, 152, '2014-05-07 15:56:59'),
(97, 5588145, 121, 155, '2014-05-07 15:57:00'),
(98, 5588145, 121, 155, '2014-05-07 15:57:03'),
(99, 5588145, 121, 154, '2014-05-07 15:57:04');

-- --------------------------------------------------------

--
-- Table structure for table `Rules`
--

CREATE TABLE IF NOT EXISTS `Rules` (
  `Rule_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Rule_Name` varchar(45) NOT NULL,
  `Point` int(11) NOT NULL,
  `Duration` date DEFAULT NULL,
  `Active` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`Rule_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=156 ;

--
-- Dumping data for table `Rules`
--

INSERT INTO `Rules` (`Rule_ID`, `Rule_Name`, `Point`, `Duration`, `Active`) VALUES
(19, 'Dress properly', 50, '0000-00-00', 0),
(24, 'Get a perfect attendance for a course', 100, '0000-00-00', 0),
(25, 'Participate as a tutor in the faculty''s Tutor', 100, '2014-04-22', 0),
(44, 'Follow the API strictly', 5000, '0000-00-00', 0),
(79, 'Get A in IR class', 4992, '0000-00-00', 0),
(80, 'Have good-looking face', 2000, '0000-00-00', 0),
(82, 'Testing rule number', 999, '0000-00-00', 0),
(85, 'Use elevator', -10, '0000-00-00', 0),
(86, 'Use elevator', -10, '0000-00-00', 0),
(87, 'Testing rule number', 999, '0000-00-00', 0),
(88, 'Testing rule numbersdfsdfds', 9993, '2014-05-04', 0),
(89, 'Dress properly', 50, '0000-00-00', 0),
(90, 'Dress properly', 55, '0000-00-00', 0),
(91, 'A', 0, '0000-00-00', 0),
(92, 'A', 1, '0000-00-00', 0),
(93, 'Aa3', 3, '2014-05-05', 0),
(94, 'a', 5, '0000-00-00', 0),
(95, 'a', 5, '0000-00-00', 0),
(96, 'a1', 6, '0000-00-00', 0),
(97, 'a1', 65, '0000-00-00', 0),
(98, 'a1', 65, '2014-05-06', 0),
(99, 'A', 1, '2014-05-05', 0),
(100, 'A', 1, '0000-00-00', 0),
(101, 'A@', 1, '0000-00-00', 0),
(102, 'A@.<', 1, '0000-00-00', 0),
(103, 'A@.<', 1, '0000-00-00', 0),
(104, 'A@.<', 2, '2014-05-04', 0),
(105, 'A', 1, '2014-05-05', 0),
(106, 'A', 1, '2014-05-05', 0),
(107, 'A', -5, '2014-05-04', 0),
(108, 'A', -5, '0000-00-00', 0),
(109, 'A', -5, '0000-00-00', 0),
(110, 'A', -5, '2014-05-05', 0),
(111, 'B', 1, '2014-05-05', 0),
(112, 'B', 1, '0000-00-00', 0),
(113, 'B', -5, '2014-05-05', 0),
(114, 'C', 1, '2014-05-06', 0),
(115, 'A', 1, '2014-05-05', 0),
(116, 'B', 1, '2014-05-06', 0),
(117, 'B', 1, '2014-05-02', 0),
(118, 'c', 1, '2014-05-05', 0),
(119, 'c', 1, '0000-00-00', 0),
(120, 'P1: ', 1, '0000-00-00', 0),
(121, 'can be decimal', 1, '0000-00-00', 0),
(122, 'c', 1, '0000-00-00', 0),
(123, 'valid can backward', 1, '2014-05-01', 0),
(124, 'can be decimal( both )', 1, '0000-00-00', 0),
(125, 'c', 1, '2014-05-05', 0),
(126, 'c', 1, '2014-05-05', 0),
(127, 'c', 2, '2014-05-05', 0),
(128, 'a', 1, '2017-01-28', 0),
(129, 'a', 1, '2014-05-05', 0),
(130, 'a', 1, '2014-05-06', 0),
(131, 'can be decimal( both )', -100, '0000-00-00', 0),
(132, 'Get a perfect attendance for a course', 100, '2014-04-19', 0),
(133, 'Cab', 1, '0000-00-00', 0),
(134, 'Cab', 2, '0000-00-00', 0),
(135, 'sdfdsf', 43, '0000-00-00', 0),
(136, 'can be decimal( both )', 334, '0000-00-00', 0),
(137, 'sdfdsf', 43, '0000-00-00', 0),
(138, 'Follow the API strictly', 5000, '0000-00-00', 0),
(139, 'Get A in IR class', 4992, '0000-00-00', 0),
(140, 'Have good-looking face', 2000, '0000-00-00', 0),
(141, 'Cab', 2, '0000-00-00', 0),
(142, 'can be decimal( both )', 334, '0000-00-00', 0),
(143, 'sdfdsf', 43, '0000-00-00', 0),
(144, 'Follow the API strictly', 5000, '2014-06-12', 0),
(145, 'Get A in IR class', 4992, '0000-00-00', 0),
(146, 'can be decimal( both )', 334, '2014-05-05', 0),
(147, 'Have good-looking face', 2000, '2014-05-25', 0),
(148, 'Cab', 20, '2014-05-07', 0),
(149, 'sdfdsf', 43, '2014-05-07', 0),
(150, 'ITAPP project', 360, '2014-06-03', 1),
(151, 'YED PED', 200, '2014-06-03', 0),
(152, 'Get A in IR class', 4992, '2014-05-13', 1),
(153, 'Dress Well', 500, '2014-06-06', 1),
(154, 'Be on Time', 250, '2014-06-06', 1),
(155, 'Get Maximum on any subject', 1000, '2014-06-06', 1);

-- --------------------------------------------------------

--
-- Table structure for table `RulesLog`
--

CREATE TABLE IF NOT EXISTS `RulesLog` (
  `Transaction_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Rule_ID` int(11) NOT NULL,
  `Admin_ID` int(11) NOT NULL,
  `transactionTimestamp` datetime NOT NULL,
  `Action` varchar(10) NOT NULL,
  `newRuleID` int(11) DEFAULT NULL,
  PRIMARY KEY (`Transaction_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=277 ;

--
-- Dumping data for table `RulesLog`
--

INSERT INTO `RulesLog` (`Transaction_ID`, `Rule_ID`, `Admin_ID`, `transactionTimestamp`, `Action`, `newRuleID`) VALUES
(239, 131, 5488003, '2014-05-04 15:32:05', 'Edit', 136),
(241, 135, 5488003, '2014-05-04 15:32:31', 'Edit', 137),
(243, 44, 5488003, '2014-05-04 15:32:36', 'Edit', 138),
(245, 79, 5488003, '2014-05-04 15:32:38', 'Edit', 139),
(247, 80, 5488003, '2014-05-04 15:32:39', 'Edit', 140),
(249, 134, 5488003, '2014-05-04 15:32:43', 'Edit', 141),
(251, 136, 5488003, '2014-05-04 15:32:46', 'Edit', 142),
(253, 137, 5488003, '2014-05-04 15:32:48', 'Edit', 143),
(255, 138, 5488003, '2014-05-04 15:32:52', 'Edit', 144),
(257, 139, 5488003, '2014-05-04 15:32:53', 'Edit', 145),
(259, 142, 5488003, '2014-05-04 15:33:07', 'Edit', 146),
(261, 140, 5488003, '2014-05-04 15:52:04', 'Edit', 147),
(263, 141, 5488003, '2014-05-04 15:52:30', 'Edit', 148),
(265, 143, 5488003, '2014-05-04 15:52:45', 'Edit', 149),
(266, 144, 5488003, '2014-05-04 16:19:23', 'Delete', 0),
(267, 149, 5488003, '2014-05-04 16:22:42', 'Delete', 0),
(268, 147, 5488003, '2014-05-04 16:26:19', 'Delete', 0),
(269, 150, 5488003, '2014-05-04 17:11:21', 'Add', 150),
(270, 151, 5488003, '2014-05-04 17:11:37', 'Add', 151),
(272, 145, 5488003, '2014-05-06 20:00:17', 'Edit', 152),
(273, 151, 5488003, '2014-05-07 22:51:19', 'Delete', 0),
(274, 153, 5488003, '2014-05-07 22:51:37', 'Add', 153),
(275, 154, 5488003, '2014-05-07 22:52:14', 'Add', 154),
(276, 155, 5488003, '2014-05-07 22:52:45', 'Add', 155);

-- --------------------------------------------------------

--
-- Table structure for table `Students`
--

CREATE TABLE IF NOT EXISTS `Students` (
  `Student_ID` int(11) NOT NULL,
  `Bann_ID` varchar(45) NOT NULL,
  `Year` varchar(45) NOT NULL,
  `Track` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`Student_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Students`
--

INSERT INTO `Students` (`Student_ID`, `Bann_ID`, `Year`, `Track`) VALUES
(5388200, '1', '4th', 'SE'),
(5488003, '1', '3rd', 'DB'),
(5488022, '3', '3rd', 'MM'),
(5488066, '7', '3rd', 'EBIZ'),
(5488068, '7', '3rd', 'SE'),
(5488079, '8', '3rd', 'CS'),
(5488115, '2', '3rd', 'Management Information System'),
(5488126, '3', '3rd', 'MM'),
(5488128, '3', '3rd', 'CN'),
(5488213, '2', '3rd', 'DB'),
(5488289, '9', '3rd', 'SE'),
(5488999, '4', '3', NULL),
(5588145, '5', '2nd', NULL),
(5688100, '1', '1st', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `TicketComment`
--

CREATE TABLE IF NOT EXISTS `TicketComment` (
  `ticket_comment_id` int(11) NOT NULL AUTO_INCREMENT,
  `ticket_id` int(11) NOT NULL,
  `User_ID` int(11) NOT NULL,
  `ticket_comment_message` longtext NOT NULL,
  `ticket_comment_added` datetime NOT NULL,
  PRIMARY KEY (`ticket_comment_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `TicketComment`
--

INSERT INTO `TicketComment` (`ticket_comment_id`, `ticket_id`, `User_ID`, `ticket_comment_message`, `ticket_comment_added`) VALUES
(1, 4, 5488115, 'Test Test Test Test', '2014-05-04 16:29:12'),
(2, 5, 5488115, 'Please reconsider my request<br />\n<br />\nI have my own reason for getting a good reward from you<br />\n<br />\nThank you', '2014-05-04 17:08:06'),
(3, 4, 5488115, 'This is a third comment on the fourth ticket', '2014-05-04 17:15:09'),
(4, 1, 5488115, 'Test comment on the first ticket', '2014-05-04 17:17:50'),
(5, 5, 1, 'Shut up bro!', '2014-05-06 03:55:12'),
(6, 3, 1, 'Hello World!<br />\n<br />\nNot Kitty!<br />\n<br />\nI don''t like Kitty!', '2014-05-06 03:55:51'),
(7, 7, 5488003, 'Bravo por mueng', '2014-05-06 04:14:52'),
(8, 3, 5488115, 'WHATTTTTTTTTTTTTTTTT<br />\n<br />\nTHE HECK!!', '2014-05-06 20:38:08'),
(9, 3, 1, 'Shame on youuuuuu', '2014-05-06 20:39:07'),
(10, 8, 5488003, 'It''s Macdonald!<br />\n<br />\nDamn you!!!', '2014-05-07 10:50:57'),
(11, 8, 5488003, 'Oops, McDonald!!! My mistake.', '2014-05-07 10:51:19'),
(12, 9, 1, 'No, I can''t.<br />\n<br />\nGoodbye man.', '2014-05-07 11:02:31'),
(13, 7, 5488003, 'HAYOOO', '2014-05-07 11:14:17'),
(14, 10, 5488003, 'YO', '2014-05-07 11:18:02'),
(15, 7, 5488003, 'Thanks.', '2014-05-07 22:53:22');

-- --------------------------------------------------------

--
-- Table structure for table `Tickets`
--

CREATE TABLE IF NOT EXISTS `Tickets` (
  `ticket_id` int(11) NOT NULL AUTO_INCREMENT,
  `By_User_ID` int(11) NOT NULL,
  `To_User_ID` int(11) NOT NULL,
  `ticket_topic` varchar(200) NOT NULL,
  `ticket_message` longtext NOT NULL,
  `ticket_added` datetime NOT NULL,
  `ticket_status` varchar(10) NOT NULL DEFAULT 'open',
  PRIMARY KEY (`ticket_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `Tickets`
--

INSERT INTO `Tickets` (`ticket_id`, `By_User_ID`, `To_User_ID`, `ticket_topic`, `ticket_message`, `ticket_added`, `ticket_status`) VALUES
(1, 5488115, 1, 'Test new ticket', 'This is a test new ticket for ticketing system', '2014-05-03 22:20:16', 'open'),
(2, 5488115, 2, 'Another ticket', 'This is another ticket message for testing the ticketing system.', '2014-05-03 22:31:28', 'close'),
(3, 5488115, 1, 'This is a third ticket right hereee', 'Hello Kitty\n\nThis is the third ticket\n\nHappy Summer!', '2014-05-03 22:33:47', 'open'),
(4, 5488115, 2, 'Fourth tickets', 'This is fourth ticket with different receiver', '2014-05-03 22:35:56', 'open'),
(5, 5488115, 1, 'Need some help about my reward', 'I think I deserve to get some reward\n\nUntil now, my point is still 0\n\nPlease consider me again\n\nThank you', '2014-05-04 02:43:21', 'close'),
(6, 5488115, 1, 'Testing Testing', 'Hi Bob<br />\n<br />\nThis is a test, eiei<br />\n<br />\nLet it goooooooooooooooooooooooo', '2014-05-04 02:44:25', 'open'),
(7, 5488115, 5488003, 'Bravo!', 'Bravo Admin!<br />\n<br />\nHave a great day!!', '2014-05-06 04:14:25', 'open'),
(8, 5488115, 5488003, 'I''m lovin it', 'I love KFC!!!<br />\n<br />\nDing Ding Ding', '2014-05-07 10:50:15', 'close'),
(9, 5488213, 1, 'Hello Bob!', 'Hello Bob!<br />\n<br />\nCould you remember me?', '2014-05-07 11:02:06', 'open'),
(10, 5488022, 5488003, 'Good Afternoon', '>w<', '2014-05-07 11:17:16', 'open');

-- --------------------------------------------------------

--
-- Table structure for table `Users`
--

CREATE TABLE IF NOT EXISTS `Users` (
  `User_ID` int(11) NOT NULL AUTO_INCREMENT,
  `User_Name` varchar(45) NOT NULL,
  `User_Surname` varchar(45) NOT NULL,
  `Password` varchar(45) NOT NULL,
  `User_Email` varchar(45) DEFAULT NULL,
  `User_Tel` varchar(45) DEFAULT NULL,
  `Restriction` char(1) NOT NULL,
  PRIMARY KEY (`User_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5588146 ;

--
-- Dumping data for table `Users`
--

INSERT INTO `Users` (`User_ID`, `User_Name`, `User_Surname`, `Password`, `User_Email`, `User_Tel`, `Restriction`) VALUES
(1, 'Bob', 'Swagger', 'instructor', 'SFecousels1960@einrot.com', '202-555-019', 'I'),
(2, 'Aston', 'Martin', 'instructor', 'ZUnely8241@cuvox.de', '202-555-0113', 'I'),
(121, 'Peter', 'Parker', 'instructor', 'p.parker@gmail.com', '0832154478', 'I'),
(5388200, 'Preeya', 'Wangthong', 'student', 'GBleme1928@dayrep.com', '202-555-0168', 'S'),
(5488003, 'Pana', 'Amphaisakul', 'administrator', 'Congly9718@fleckens.hu', '202-555-0157', 'A'),
(5488022, 'Jenneth', 'Chaovisutikul', 'student', 'jane.chao@hotmail.com', '081234723', 'S'),
(5488066, 'Sirachat', 'Kongkasae', 'student', 'sirachat.kong@hotmail.com', '0823243528', 'S'),
(5488068, 'Yada', 'Vittayabandit', 'student', 'mind.m@hotmail.com', '082352349', 'S'),
(5488079, 'Noppawit', 'Hansompob', 'student', 'noppawit.han@email.com', '082352345', 'S'),
(5488115, 'Nutchanon', 'Phongoen', 'student', 'earthamp@gmail.com', '081-4804553', 'S'),
(5488126, 'Wanchalerm', 'Popee', 'student', 'lerm.pee@pee.net', '0834567890', 'S'),
(5488128, 'Pawit', 'Pornkitprasan', 'student', 'pawitp@email.com', '0823465435', 'S'),
(5488213, 'Parit', 'Waitayakomol', 'student', 'pascmo24234@edbiz.com', '081234723', 'S'),
(5488289, 'Pattarapond', 'Sukvanachaikul', 'student', 'pat.p@email.com', '082362352', 'S'),
(5488999, 'Ryan', 'Glossling', 'student', 'r.glossling@gmail.com', '0842545566', 'S'),
(5588145, 'Evans', 'Romanov', 'student', 'Evan@gmail.com', '0898755464', 'S');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<?php
require_once "lib/nusoap.php";

function createConnection(){
	// Create connection
	include "connection/databaseConnection.php";
	$con=mysqli_connect($host,$username,$password,$db_name);
	//$con=mysqli_connect("localhost","root","root","RewardDatabase");
	// Check connection
	if (mysqli_connect_errno()){
		return "Failed to connect to MySQL: " . mysqli_connect_error();
	}
	return $con;
}

function login($username, $password)
{
	$con = createConnection();
	$users_sql = "SELECT CONCAT(User_Name, User_ID) AS Username , Password, User_ID FROM Users";
	$result_users = mysqli_query($con, $users_sql);
	mysqli_close($con);
	while($user = mysqli_fetch_array($result_users))
	{
		if($username == $user['Username'] && $password == $user['Password'])
		{
			return $user['User_ID'];
		}
	}
	return -1;
}

function editProfile($userid, $year, $track, $email, $tel)
{
	$con = createConnection();
	$userquery = "UPDATE `Users` SET `User_Email`=\"". $email ."\",`User_Tel`=\"". $tel ."\" WHERE User_ID = \"" . $userid ."\"";
	$studentquery = "UPDATE `Students` SET `Year`=\"". $year ."\",`Track`=\"". $track ."\" WHERE Student_ID = " . $userid;
	mysqli_query($con, $userquery);
	mysqli_query($con, $studentquery);
	mysqli_close($con);
	return $userquery;
}

function authorize($userid)
{
	$con = createConnection();
	$sql = "SELECT Restriction FROM Users WHERE User_ID = " . $userid;
	$result = mysqli_query($con, $sql);
	$user = mysqli_fetch_array($result);
	return $user['Restriction'];
}

function getUserInfo($userid)
{	
	$con = createConnection();
    $sql="SELECT * FROM Users WHERE User_ID = ".$userid;
    $result = mysqli_query($con,$sql);
	while ($rs1 = mysqli_fetch_array($result)) {
		if($rs1['Restriction']=='S')
		{
			$sql="SELECT * FROM Students s, Bann b where s.Bann_ID = b.Bann_ID AND Student_ID = ".$userid;
			$result = mysqli_query($con,$sql);
			while($rs2 = mysqli_fetch_array($result))
			{
				$ret = array("id"=>$userid,"name"=>$rs1['User_Name'],"surname"=>$rs1['User_Surname'],"email"=>$rs1['User_Email'],"tel"=>$rs1['User_Tel'],"track"=>$rs2['Track'],"year"=>$rs2['Year'],"bann"=>$rs2['Bann_Name'],"b_id"=>$rs2['Bann_ID']);
			}
		}
		else
			$ret = array("id"=>$userid,"name"=>$rs1['User_Name'],"surname"=>$rs1['User_Surname'],"email"=>$rs1['User_Email'],"tel"=>$rs1['User_Tel']);
	}
    mysqli_close($con);
	return $ret;
}

function getStudentScore($std){
	$con = createConnection();
	$result = mysqli_query($con,"SELECT Rewards.Student_ID,Students.Bann_ID, Rules.Point FROM Rewards INNER join Students on Rewards.Student_ID = Students.Student_ID INNER join Rules on Rewards.Rule_ID = Rules.Rule_ID");

	$score = 0;
	while($row = mysqli_fetch_array($result))
	{

		if($row['Student_ID'] == $std){
			$score = $score+$row['Point'];
		}
	
	}
	mysqli_close($con);
	return $score;
}

function getBannScore($bann){
	$con = createConnection();
	$result = mysqli_query($con,"SELECT Rewards.Student_ID, Students.Bann_ID, Rules.Point FROM Rewards INNER join Students on Rewards.Student_ID = Students.Student_ID INNER join Rules on Rewards.Rule_ID = Rules.Rule_ID");
	$score = 0;
	while($row = mysqli_fetch_array($result))
	{

		if($row['Bann_ID'] == $bann){
			$score = $score+$row['Point'];
		}
		
	}
	mysqli_close($con);
	return $score;
}

function getRewardHistory($studentID)
{
	$con = createConnection();
	$query = "SELECT Rewards.Reward_ID, Students.Student_ID,
	CONCAT(S.User_Name,' ',S.User_Surname) AS Student_Name,
	Rewards.Instructor_ID,
	CONCAT(I.User_Name,' ',I.User_Surname) AS Instructor_Name,
	Rules.Rule_ID, Rules.Rule_Name, Rules.Point, Rewards.Time_Stamp
	FROM Rewards
	INNER JOIN Students ON Rewards.Student_ID = Students.Student_ID 
	INNER JOIN Rules ON Rewards.Rule_ID = Rules.Rule_ID 
	INNER JOIN Users S ON Rewards.Student_ID = S.User_ID
	INNER JOIN Users I ON Rewards.Instructor_ID = I.User_ID
	WHERE Students.Student_ID =  \"". $studentID ."\" ORDER By Rewards.Time_Stamp DESC 
	LIMIT 0,10";
	$result = mysqli_query($con,$query);
	return parseRewardHistory($result);
}

function parseRewardHistory($result){
	$jsonResult = "[";
	$i = 0;
	while($row = mysqli_fetch_array($result))
	{
		if($i!=0)
			$jsonResult .= ", ";
		$jsonResult .= "{";
		$jsonResult .= ' "Reward_ID" : "' . $row['Reward_ID'] . '", ';
		$jsonResult .= ' "Student_ID" : "' . $row['Student_ID'] . '", ';
		$jsonResult .= ' "Student_Name" : "' . $row['Student_Name'] . '", ';
		$jsonResult .= ' "Instructor_ID" : "' . $row['Instructor_ID'] . '", ';
		$jsonResult .= ' "Instructor_Name" : "' . $row['Instructor_Name'] . '", ';
		$jsonResult .= ' "Rule_ID" : "' . $row['Rule_ID'] . '", ';
		$jsonResult .= ' "Rule_Name" : "' . $row['Rule_Name'] . '", ';
		$jsonResult .= ' "Point" : "' . $row['Point'] . '", ';
		$jsonResult .= ' "Timestamp" : "' . $row['Time_Stamp'] . '"';
		$jsonResult .= "}";
		$i++;
	}
	$jsonResult .= "]";
	return $jsonResult;
}

$server = new soap_server();
$server->register("login");
$server->register("authorize");
$server->register("getUserInfo");
$server->register("getStudentScore");
$server->register("getBannScore");
$server->register("getRewardHistory");
$server->register("editProfile");

if(!isset($HTTP_RAW_POST_DATA))
	$HTTP_RAW_POST_DATA = file_get_contents('php://input');

$server->service($HTTP_RAW_POST_DATA);

?>
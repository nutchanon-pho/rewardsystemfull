function getAvatar() {
	$('#user_avt').html("<img src=\"img/loading.gif\" width=\"32\" height=\"32\" border=\"0\" />");
	$.get("getavt.php",{ cache: false }, function(callback) {
		$('#user_avt').html(callback);
	});
}

$(document).ready(function() {
	
	getAvatar();
	
	var upload_valid = false;
	$(document).on('click', '#up_avatar', function(e) {
		e.preventDefault();
		$('#avt_preparing').show();
		var tempdata = new FormData($('#avt_upload_form')[0]);
		//console.log("Preparing...");
		$.ajax({
			type: 'post',
			url: 'avtuploadcheck.php',
			data: tempdata,
			cache: false,
			contentType: false,
			processData: false,
			success: function(callback) {
				$('#avt_preparing').hide();
				if (callback != "200") {
		        	$('#avt_err').html(callback);
		        	$('#avt_error').slideDown(300);
		        	setTimeout(function() {
		        		$('#avt_error').slideUp(300);
		        	}, 4000);
		        } else {
		        	upload_valid = true;
		        	$('#avt_progress').show();
		        	$('#avt_upload_form').submit();
		        }
			}
		});
		
	});
	
	$('#avt_upload_form').ajaxForm({
		beforeSend: function() {
			//console.log("beforeSend");
			$('#avt_percent').html('0');
			$('#avt_progressbar').attr('aria-valuenow', '0');
			$('#avt_progressbar').css('width', '0%');
		},
		uploadProgress: function(event, position, total, percent) {
			//console.log("uploadProgress");
			$('#avt_percent').html(percent);
			$('#avt_progressbar').attr('aria-valuenow', percent);
			$('#avt_progressbar').css('width', percent + '%');
		},
		success: function(callback) {
			//console.log("Success");
			if (callback != "200") {
			   	$('#avt_err').html(callback);
			       $('#avt_error').slideDown(300);
			       setTimeout(function() {
			       	$('#avt_error').slideUp(300);
			       }, 4000);
			} else {
				$('#user_avt').html("<img src=\"img/loading.gif\" width=\"32\" height=\"32\" border=\"0\" />");
				setTimeout(function() {
					getAvatar();
				}, 500);
			   	$('#avatar_upload_modal').modal('hide');
			   	$('#avt_progress').hide();
			}
			
	    }
	});
});

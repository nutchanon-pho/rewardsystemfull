<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Reward System</title>

		<!-- Bootstrap -->
		<link href="css/fontface.css" rel="stylesheet">
		<link href="css/bootstrap.css" rel="stylesheet">
		<link href="css/style.css" rel="stylesheet">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
		<style>
			.col-centered	{
			    float: none;
			    margin: 0 auto;
			    clear: left;
			}
		</style>
	</head>
	<body>
	<?php
	$page = "leaderboard"; 
	include "nav.php";
	require_once "lib/nusoap.php";


	include "connection/serverPath.php";
	$client = new nusoap_client($serverPath . "leaderboard_server.php",false);
	
	$rank_student = false;
	$rank_baan = false;
	$rank_monthly = false;
	if (isset($_GET['rank'])) {
		if (strtolower($_GET['rank']) == "baan") {
			$rank_baan = true;
		} else if (strtolower($_GET['rank']) == "student") {
			$rank_student = true;
		} else if (strtolower($_GET['rank']) == "monthly") {
			$rank_monthly = true;
		}

	} else {
		$rank_student = true;
	}
	
	//$client = new nusoap_client("http://localhost:8888/reward/leaderboard_server.php",false);
	$result = "";
	if ($rank_student) {
		$result = $client->call("getLeaderboard",array());
	}
	else if ($rank_monthly) {
		$result = $client->call("getLeaderboard_monthly",array());		
	}
	
	$result_bann = "";
	if ($rank_baan) {
		$result_bann = $client->call("getLeaderboard_bann",array());		
	}
	
	
	
	$error2 = $client->getError();
	if($error2){
		echo "<h2>error2<h2><pre>".$error2."</pre>";
	}
	else{
	?>
	
	<?php if ($rank_baan) : ?>
		<div class="container">
			<div class="box">
				<h1><span class="glyphicon glyphicon-th-list"> </span> Leader Board <small>Baan Ranking</small></h1>
				<hr />
				<?php echo $result_bann?>
			</div>
		</div>
	<?php elseif ($rank_student) : ?>
		
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-lg-11 col-centered">
					<div class="box">
						<h1><span class="glyphicon glyphicon-th-list"> </span> Leader Board <span class="hidden-lg"><br /></span><small>TOP 10 Student Ranking</small></h1>
					</div>
				</div>
				<div class="col-lg-1">&nbsp;</div>
			</div>
		</div>
		
		<?php echo $result; ?>
	
	<?php elseif ($rank_monthly) : ?>
		
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-lg-11 col-centered">
					<div class="box">
						<h1><span class="glyphicon glyphicon-th-list"> </span> Leader Board <span class="hidden-lg"><br /></span><small>TOP 10 Student Ranking of this month</small></h1>
					</div>
				</div>
				<div class="col-lg-1">&nbsp;</div>
			</div>
		</div>
		
		<?php echo $result; ?>
		
	<?php endif; ?>

		<div class="container box" id="footer">
			&copy; Copyright 2014 <em>Reward System</em>. All Rights Reserved.
		</div>
		<a href="#" id="scroll_top">Scroll</a>

		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
		<!-- Include all compiled plugins (below), or include individual files as needed -->
		<script src="js/bootstrap.min.js"></script>
		<script src="js/scroll.js"></script>
		<?php if ($rank_baan) : ?>
		<script>
			$('#rule_option a:first').tab('show');
			for(var i=1;i<=10;i++)
			{
				$("#Baan" + i +"Table").hide();
			}

			<?
				for($i=1;$i<=10;$i++)
				{
					echo '$("#Baan" + '.$i.').click(function(){
					$("#Baan" + '.$i.' +"Table").toggle(300);
				});';
				}
			?>
		</script>
		<?php endif; ?>
		<?php } ?>
	</body>
</html>
<?php session_start();
include "login_session_validator.php";
if($_SESSION['authentication'] != true || $_SESSION['permissionLevel'] != "I") //Not login yet
{
	$_SESSION['errorMsg'] = "empty";
	header("location: login.php");
	exit();
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Reward System</title>

	<!-- Bootstrap -->
	<link href="css/fontface.css" rel="stylesheet">
	<link href="css/bootstrap.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	<script>
			/*
			 * Rewards
			 */
			 function searchFunc()
			 {
			 	var searchBy = document.getElementById("searchBy").value;
			 	var keyword = document.getElementById("keyword").value;
			 	var search = document.getElementById("Search").value;
			 	if(keyword != ""){
			 		$('#search_head').removeClass('hide');
			 		var xmlhttp=new XMLHttpRequest();
			 		xmlhttp.onreadystatechange=function()
			 		{
			 			if (xmlhttp.readyState==4 && xmlhttp.status==200)
			 			{
			 				document.getElementById("showQuery").innerHTML=xmlhttp.responseText;
			 			}
			 		}
			 		xmlhttp.open("GET","reward_client.php?keyword="+keyword+
			 			"&searchBy="+searchBy+
			 			"&search="+search,true);

			 		xmlhttp.send();
			 	}
			 	else alert("please fill in the keyword");
			 }
			 function giveRewardFunc(rid)
			 {
			 	var instructorID = document.getElementById("instructor_id").value;
			 	var studentID = document.getElementById("student_id").value;
			var ruleID = rid; //document.getElementById("rule_id").value;
			var giveReward = document.getElementById("giveReward").value;

			if(instructorID != "")
			{
				var xmlhttp=new XMLHttpRequest();
				
				xmlhttp.onreadystatechange=function()
				{
					if (xmlhttp.readyState==4 && xmlhttp.status==200)
					{
						document.getElementById("showResponse").innerHTML=xmlhttp.responseText;
						$('#reward_success_label').slideDown(300);
						
						setTimeout(function() {
							$("#reward_success_label").slideUp(300);
						}, 3000);
					}
				}
				xmlhttp.open("GET","reward_client.php?giveReward="+giveReward+
					"&rule_id="+ruleID+
					"&student_id="+studentID+
					"&instructor_id="+instructorID,true);
				
				xmlhttp.send();
			}
			else alert("please fill in your InstructorID");
		}
		function buttonClick(id, name, sname){
			$("#student_id").val(id);
			$("#ruleList").show();
			$('#fname').html(name);
			$('#lname').html(sname);
			
		}




		$(document).ready(function() {

				// Rewards
				$("#addReward").hide();
				$("#ruleList").hide();
				$("#reward_success_label").hide();
				
				// Rules
				$("#addForm").hide();
				$("#editForm").hide();

				$("#addrule").click(function() {
					$("#addForm").show("slow");
					 //document.getElementById("rule_name").value = "";
					 //document.getElementById("point").value = "";
					 //document.getElementById("range").value = "";
					 //$("#editForm").hide("slow");
					 $('#add_rule_modal').modal();
					});

				/* Edit Profile */
				$(".editProfileForm").hide();
				$("#editProfile").click(function(){
					$(".editProfileForm").show();
					$(".profileInfo").hide();
				});
				$("#saveChanges").click(function(){
					$(".editProfileForm").hide();
					$(".profileInfo").show();
					var param = {};
					param['editProfile'] = true;
					param['userID'] = $("#userID").html();;
					param['email'] = $("#editEmail").val();
					param['tel'] = $("#editTel").val();
					$.post("userprofile_client.php",param,function(data, status){
						location.reload();
					});
				});
			});
</script>
<script src="js/bootstrap.min.js"></script>
<script src="js/scroll.js"></script>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body onload="showResult()">

		<?php
		$page = "profile";
		include "login_session_validator.php";
		include "nav.php";
		include "avtupload_popup.php";
		require_once "lib/nusoap.php";

		include "connection/serverPath.php";
		$client = new nusoap_client($serverPath . "userprofile_server.php",false);
		//$client = new nusoap_client("http://localhost:8888/reward/userprofile_server.php",false);

		if(isset($_SESSION['authentication']) && $_SESSION['authentication'] == true)//Valid user
		{
			$userid = $_SESSION['userid'];
			$permissionLevel = $_SESSION['permissionLevel'];
			$result = $client->call("getUserInfo",array("uid"=>$userid));
		}
		?>

		<div class="container">
			<div class="row">

				<div class="col-sm-12 col-md-3 col-lg-3">
					<div class="box">
						<div class="row">
							<div class="col-xs-4 col-sm-4 col-md-4 col-lg-12">
								<center id="user_avt">
									<img src="img/loading.gif" width="32" height="32" border="0" />
								</center>
							</div>
							<div class="col-xs-8 col-sm-8 col-md-8 col-lg-12">
								<span class="visible-lg"><br /></span>
								<p class="lead"><strong><?echo $result["name"]." ".$result["surname"];?></strong></p>
								<p><span class="label label-success">Instructor</span></p>
							</div>
						</div>
						<br />
						<div>
							<form action="login.php" method="post">
								<input type="hidden" name="signout" value="true">
								<center><button type="submit" class="btn btn-xs btn-danger">sign out</button></center>
							</form>
						</div>
					</div>
				</div>

				<div class="col-sm-12 col-md-9 col-lg-9">
					<div class="box">
						<h1><span class="glyphicon glyphicon-user"> </span> Instructor / <?echo $result["name"]." ".$result["surname"];?> <!-- Single button -->
							<div class="btn-group">
								<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
									Edit <span class="caret"></span>
								</button>
								<ul class="dropdown-menu dropdown-normal" role="menu">
									<li>
										<a id="editProfile">Profile</a>
									</li>
									<li>
										<a href="#" data-toggle="modal" data-target="#avatar_upload_modal">Avatar</a>
									</li>
								</ul>
							</div></h1>

							<hr />
							<ul class="nav nav-tabs" data-tabs="tabs" id="rule_option">
								<li class="active">
									<a href="#profile" data-toggle="tab">Profile</a>
								</li>
								<li>
									<a href="#search_student" data-toggle="tab">Rewards</a>
								</li>

							</ul>
							<br/>

							<div class="tab-content">
								<div class="tab-pane active" id="profile">
									<div class="table-responsive">
										<table class="table table-bordered">
											<tr>
												<td colspan="2" class="td-panel"><h4>&nbsp;&nbsp;Profile</h4></td>
											</tr>
											<tr>
												<td style="width: 15%;">Instructor ID:</td>
												<td id="userID"><?php echo $result["id"];?></td>
											</tr>
											<tr>
												<td style="width: 15%;">Email:</td>
												<td>
													<span class="profileInfo"><?php echo $result["email"];?></span>
													<div class="input-group editProfileForm" >
														<input type="text" class="form-control" id="editEmail" placeholder="Email" value="<?php echo $result["email"];?>">
													</div>
												</td>
											</tr>
											<tr>
												<td style="width: 15%;">Tel:</td>
												<td>
													<span class="profileInfo"><?php echo $result["tel"];?></span>
													<div class="input-group editProfileForm" >
														<input type="text" class="form-control" id="editTel" placeholder="Telephone" value="<?php echo $result["tel"];?>">
													</div>
												</td>
											</tr>
										</table>
									</div>
								</div>
								<div class="tab-pane" id="search_student">
								</br>
								<div class="form-inline">
									<div class="form-group">
										<select id="searchBy" class="form-control">
											<option value="studentID">StudentID</options> <option value="name">Name</options> <option value="surname">Surname</options> <option value="bann">Bann</options>
											</select>
										</div><!-- /input-group -->
										<div class="form-group">
											<input type="text" class="form-control" id="keyword" name="keyword">
											<input type="hidden" id="Search" value="Search" />
											<button class="btn btn-default" name="search" onclick="searchFunc()"><span class="glyphicon glyphicon-search"></span> Search</button>
										</div><!-- /input-group -->
									</div>
								</br>
								<div class="panel panel-default">
									<!-- Default panel contents -->
									<div class="panel-heading hide" id="search_head">
										<h4>&nbsp;&nbsp;<span class="glyphicon glyphicon-list-alt"> </span> Search Result</h4>
									</div>

									<!-- Table -->
									<div id="showQuery"></div>
									<div id="addReward">
										Instructor ID: <input type="text" class="form-control" id="instructor_id" value =<?php echo $_SESSION['userid'];?> readonly="readonly"/><br>
										Student ID: <input type="text" class="form-control" id="student_id" readonly="readonly" /><br>
									</div>


									<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
										<div class="modal-dialog modal-lg">
											<div class="modal-content">

												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
														&times;
													</button>
													<h4 class="modal-title" id="myLargeModalLabel"><h4>&nbsp;&nbsp;<span class="glyphicon glyphicon-star"></span> Give Reward</h4>
												</div>
												<div class="modal-body">

													<div class="alert alert-success" id="reward_success_label">
														<strong>Completed!</strong> <span id="showResponse"></span>
													</div>

													<div class="panel panel-default">
														<!-- Default panel contents -->
														<div class="panel-heading">
															<!--a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"--><a href="#"> <h3><span id="fname"></span> <span id="lname"></span></h3> </a>
														</div>

														<!-- reward -->
														<div class="panel-heading">
															<h4>Please select rule</h4>
														</div>
														

														
														<div class="panel-body">

															<!-- Table -->
															<div class="table-responsive">
																<table class="table">
																	<thead>
																		<tr>
																			<th>Rule Name</th>
																			<th>Point</th>
																			<th>Action</th>
																		</tr>
																	</thead>
																	<tbody>
																		<?php
																	//$con = mysqli_connect("localhost","root","root","RewardDatabase");
																		
																		include "connection/databaseConnection.php";
																		$con=mysqli_connect($host,$username,$password,$db_name);
																		
																		// Check connection
																		if (mysqli_connect_errno()){
																			echo "Failed to connect to MySQL: " . mysqli_connect_error();
																		}
																		
																		$query= "SELECT Rule_ID,Rule_Name,Point FROM Rules WHERE Active = 1 AND NOW()<=Duration";
																		$result = mysqli_query($con,$query);
																		mysqli_close($con);
																		//echo "<select id = 'rule_id'>";
																		while($row = mysqli_fetch_array($result))
																		{
																			echo"<tr>\n";
																			echo"	<td>".$row['Rule_Name']."</td>\n";
																			echo"	<td>".$row['Point']."</td>\n";
																			echo"	<td>\n";
																			echo"		<button type=\"button\" class=\"btn btn-success btn-sm\" id=\"giveReward\" value=\"giveReward\" onclick=\"giveRewardFunc(".$row['Rule_ID'].")\">\n";
																			echo"			<span class=\"glyphicon glyphicon-star\"></span> Give reward\n";
																			echo"		</button>\n";
																			echo"	</td>\n";
																			echo"</tr>\n";
																		}
																		//echo "</select>";
																		?>

																	</tbody>
																</table>
															</div>
														</div><!--end rule -->
													</div>
												</div><!-- end modal body-->
											</div>
										</div>
									</div><!-- end modal-->
								</div>
							</div><!-- end student search -->
						</div><!-- end panel-->
						<form action="login.php" method="POST" style="display:inline;">
							<input type="hidden" name="signout" value="true">
							<button type="submit" class="btn btn-danger">Sign Out</button>
						</form>
						<button type="button" class="btn btn-primary editProfileForm" id="saveChanges">Save changes</button>
					</div><!-- end show rule-->

				</div><!-- end tab-content-->
			</div>
		</div>
	</div>

</div>

</div>
</div>

<div class="container box" id="footer">
	&copy; Copyright 2014 <em>Reward System</em>. All Rights Reserved.
</div>
<a href="#" id="scroll_top">Scroll</a>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/jqueryform.js"></script>
<script src="js/avatarupload.js"></script>
</body>
</html>
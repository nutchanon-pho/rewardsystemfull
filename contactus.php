<?php
session_start();
if($_SESSION['authentication'] != true) //Not login yet
{
	$_SESSION['errorMsg'] = "empty";
	header("location: login.php");
	exit();
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Reward System</title>

	<!-- Bootstrap -->
	<link href="css/fontface.css" rel="stylesheet">
	<link href="css/bootstrap.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
</head>
<body>

	<?php 
	$page = "contact";
	include "login_session_validator.php";
	include "nav.php";
	include "avtupload_popup.php";
	require_once "lib/nusoap.php";
		
		include "connection/serverPath.php";
		$client = new nusoap_client($serverPath . "contactus_server.php",false);
		
		$client2 = new nusoap_client($serverPath + "userprofile_server.php",false);
		if(isset($_SESSION['authentication']) && $_SESSION['authentication'] == true) {
			$result = $client2->call("getUserInfo",array("uid"=>$_SESSION['userid']));
		}
		?>
		
		<div class="modal fade" id="create_new_ticket">
		  <div class="modal-dialog">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		        <h4 class="modal-title">New ticket</h4>
		      </div>
		      <div class="modal-body">
		      	<div class="alert alert-danger" id="create_ticket_error" style="display: none"><strong>Error:</strong> <span id="create_ticket_error_msg"></span></div>
		        <div class="form-group">
		        	<label class="control-label" for="ticTopic">Topic</label>
		        	<input type="text" class="form-control" id="ticTopic" maxlength="200" placeholder="Enter topic">
		        </div>
		        <div class="form-group">
		        	<label class="control-label" for="ticTo">Contact to</label>
		        	<select class="form-control" id="high_users">
		        		<option value="0" selected="selected">Please select someone</option>
		        	</select>
		        </div>
		        <div class="form-group">
		        	<label class="control-label" for="ticMsg">Message</label>
		        	<textarea class="form-control" placeholder="Enter message" id="ticMsg" rows="4"></textarea>
		        </div>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-primary" id="do_create_ticket">Create ticket</button>
		        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		      </div>
		    </div><!-- /.modal-content -->
		  </div><!-- /.modal-dialog -->
		</div><!-- /.modal -->
		
		<div class="modal fade" id="view_ticket">
		  <div class="modal-dialog">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		        <h4 class="modal-title" id="ticket_title"></h4>
		      </div>
		      <div class="modal-body">
		      	<div style="width: 100%; height: 300px; overflow: auto">
			        <div class="panel panel-default">
			        	<div class="panel-body">
			        		<div class="row">
			        			<div class="col-xs-3 col-sm-3 col-md-3 col-lg-2">
			        				<span id="tk_start_avt"><img src="img/person.jpg" class="img-responsive" /></span>
			        			</div>
			        			<div class="col-xs-9 col-sm-9 col-md-9 col-lg-10">
			        				<small><strong id="tk_sender_name"></strong></small>
			        				<p><span id="tk_msg"></span></p>
			        				<small class="pull-right" style="color: #777;" id="tk_date"></small>
			        			</div>
			        		</div>
			        	</div>
			        </div>
			        <div id="comment_lists"></div>
			    </div>
			    <small>Reply message<span id="comment_status"></span>: </small>
			    <p><textarea class="form-control" id="comment_msg" rows="3"></textarea></p>
			    <center><button class="btn btn-primary" id="send_comment">Reply</button><span id="loader"></span></center>
		      </div>
		      <div class="modal-footer">
		      	<p class="pull-left" style="color: #777;">Ticket Status: <span id="tk_status"></span><?php if ($_SESSION['permissionLevel'] == 'I' || $_SESSION['permissionLevel'] == 'A'): ?> <span id="tk_action"></span><?php endif; ?></p>
		        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		      </div>
		    </div><!-- /.modal-content -->
		  </div><!-- /.modal-dialog -->
		</div><!-- /.modal -->



		<div class="container">
			<div class="row">

				<div class="col-sm-12 col-md-3 col-lg-3">
					<div class="box">
						<div class="row">
							<div class="col-xs-4 col-sm-4 col-md-4 col-lg-12">
								<center id="user_avt">
									<img src="img/loading.gif" width="32" height="32" border="0" />
								</center>
							</div>
							<div class="col-xs-8 col-sm-8 col-md-8 col-lg-12">
								<span class="visible-lg"><br /></span>
								<p class="lead"><strong><?echo $result["name"]." ".$result["surname"];?></strong></p>
								<?php if ($_SESSION['permissionLevel'] == 'S'): ?>
								<p><span class="label label-primary"><?php echo $result["id"];?></span> <span class="label label-success"><?php echo $result['year']; ?> Year</span> <span class="label label-warning">Baan <?php echo $result['b_id']; ?></span></p>
								<?php elseif ($_SESSION['permissionLevel'] == 'I'): ?>
								<p><span class="label label-success">Instructor</span></p>
								<?php elseif ($_SESSION['permissionLevel'] == 'A'): ?>
								<p><span class="label label-primary">Administrator</span></p>
								<?php endif; ?>
							</div>
						</div>
						<br />
						<div>
							<form action="login.php" method="POST">
								<input type="hidden" name="signout" value="true">
								<center><button type="submit" class="btn btn-xs btn-danger">Sign Out</button></center>
							</form>
						</div>
					</div>
				</div>

				<div class="col-sm-12 col-md-9 col-lg-9">
					<div class="box">
						<h1><span class="glyphicon glyphicon-envelope"></span> Contact us<?php if ($_SESSION['permissionLevel'] == 'S'): ?> <button class="btn btn-lg btn-success" data-toggle="modal" data-target="#create_new_ticket"><span class="glyphicon glyphicon-plus"></span> New Ticket</button><?php endif; ?></h1>
						<hr />
						<div id="ticket_lists"></div>
						<ul class="nav nav-pills nav-stacked" id="all_tickets">
							<!--li><a href="#"><span class="badge pull-right">2</span> Test 1</a></li>
							<li><a href="#">Test 2</a></li>
							<li><a href="#"><span class="badge pull-right">3</span> Test 3</a></li>
							<li><a href="#">Test 4</a></li>
							<li><a href="#">Test 5</a></li>
							<li><a href="#"><span class="badge pull-right">4</span> Test 6</a></li-->
						</ul>
					</div>
				</div>
			</div>
			
			<div class="container box" id="footer">
			&copy; Copyright 2014 <em>Reward System</em>. All Rights Reserved.
			</div>
			<a href="#" id="scroll_top">Scroll</a>
		</div>

				
		<!-- Include all compiled plugins (below), or include individual files as needed -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/jqueryform.js"></script>
		<script src="js/scroll.js"></script>
		<script src="js/avatarupload.js"></script>
		<script>
		
			function loadTickets() {
				$('#all_tickets').html("<center><img src=\"img/loading.gif\" width=\"32\" height=\"32\" border=\"0\" /></center>");
				$.ajax({
					type: 'post',
					url: 'contactus_client.php',
					data: {
						'action' : 'loadticket'
					},
					success: function(callback) {
						
						$('#all_tickets').html("");
						if (callback == "<center>You don't have any ticket</center>") {
							$('#all_tickets').html(callback);
						} else {
							var parsedlist = $.parseJSON(callback);
							$.each(parsedlist, function(k, vl) {
								
								if (vl.type == "normal") {
									if (vl.ticket_status == "open") {
										$('#all_tickets').append("<li><a href=\"#\" class=\"theticket\" id=\"ticket-" + vl.ticket_id + "\"><span class=\"label label-success pull-right\">Open</span> " + vl.ticket_topic + " <small style=\"color: #AAA;\">To: " + vl.response_user + "</small></a></li>\n");
									} else {
										$('#all_tickets').append("<li><a href=\"#\" style=\"color: #999;\" class=\"theticket\" id=\"ticket-" + vl.ticket_id + "\"><span class=\"label label-default pull-right\">Close</span> " + vl.ticket_topic + " <small style=\"color: #AAA;\">To: " + vl.response_user + "</small></a></li>\n");
									}
								} else if (vl.type == 'high') {
									if (vl.ticket_status == "open") {
										$('#all_tickets').append("<li><a href=\"#\" class=\"theticket\" id=\"ticket-" + vl.ticket_id + "\"><span class=\"label label-success pull-right\">Open</span> " + vl.ticket_topic + " <small style=\"color: #AAA;\">From: " + vl.response_user + "</small></a></li>\n");
									} else {
										$('#all_tickets').append("<li><a href=\"#\" style=\"color: #999;\" class=\"theticket\" id=\"ticket-" + vl.ticket_id + "\"><span class=\"label label-default pull-right\">Close</span> " + vl.ticket_topic + " <small style=\"color: #AAA;\">From: " + vl.response_user + "</small></a></li>\n");
									}
								}
							});
						}
					}
				});
			}
			
			function loadComments(tid) {
				$.ajax({
					type: 'post',
					url: 'contactus_client.php',
					data: {
						'action' : 'loadcomments',
						'ticket_id' : tid
					},
					success: function(callback) {
						
						var pj = $.parseJSON(callback);
						
						var disp = "";
						
						$.each(pj, function(index, vl) {
							
							var disp_avt;
							$.ajax({
								async: false,
								url: "getavt.php?uid=" + vl.uid,
								success: function(ddtt) {
									disp_avt = ddtt;
								}
							});
							
							var disp_sender;
							$.ajax({
								async: false,
								url: "contactus_client.php?request=" + vl.uid,
								success: function(dt) {
									disp_sender = dt;
								}
							});
							
							var extd = (vl.comment_added).split(" ");
							var dates = extd[0].split("-"); 
							var times = extd[1].split(":");
							
							var d = new Date();
							d.setDate(dates[2]);
							d.setMonth(dates[1]-1);
							d.setFullYear(dates[0]);
							d.setHours(times[0]);
							
							var disp_date = d.toDateString() + ", " + d.getHours() + ":" + times[1];
							
							disp += "<div class=\"panel panel-default\">\n";
							disp += "	<div class=\"panel-body\">\n";
							disp += "		<div class=\"row\">\n";
							disp += "			<div class=\"col-xs-3 col-sm-3 col-md-3 col-lg-2\">\n";
							disp += "				" + disp_avt;
							disp += "			</div>\n";
							disp += "			<div class=\"col-xs-9 col-sm-9 col-md-9 col-lg-10\">\n";
							disp += "				<small><strong>" + disp_sender + "</strong></small>";
							disp += "				<p>" + vl.comment_msg + "</p>";
							disp += "				<small class=\"pull-right\" style=\"color: #777;\">" + disp_date + "</small>";
							disp += "			</div>\n";
							disp += "		</div>\n";
							disp += "	</div>\n";
							disp += "</div>";
						});
						
						$('#comment_lists').html(disp);
					}
				});
			}
			
			function loadHighUsers() {
				$.ajax({
					type: 'post',
					url: 'contactus_client.php',
					data: {
						'action' : 'loadhighusers'
					},
					success: function(callback) {
						//console.log(callback);
						var jparsed = $.parseJSON(callback);
						$.each(jparsed, function(key, val) {
							//console.log(val.display_name);
							$('#high_users').append("<option value=\"" + val.user_id + "\">" + val.display_name + "</option>");
						});
					}
				});
			}
			
			$(document).ready(function() {
				loadTickets();
				loadHighUsers();
				
				$(document).on('click', '#do_create_ticket', function() {
					
					if ($('#high_users').val() == 0 || !$('#ticTopic').val() || !$('#ticMsg').val()) {
						$('#create_ticket_error_msg').html("Please complete all highlighted forms below");
						$('#create_ticket_error').slideDown(300);
						setTimeout(function() {
							$('#create_ticket_error').slideUp(300);
						}, 5000);
						
						if (!$('#ticTopic').val()) {
							$('#ticTopic').parent().addClass('has-error');
						} else {
							$('#ticTopic').parent().removeClass('has-error');
						}
						
						if ($('#high_users').val() == 0) {
							$('#high_users').parent().addClass('has-error');
						} else {
							$('#high_users').parent().removeClass('has-error');
						}
						
						if (!$('#ticMsg').val()) {
							$('#ticMsg').parent().addClass('has-error');
						} else {
							$('#ticMsg').parent().removeClass('has-error');
						}
					} else {
						
						$.ajax({
							type: 'post',
							url: 'contactus_client.php',
							data: {
								'action' : 'addnewticket',
								'receiver' : $('#high_users').val(),
								'ticket_topic' : $('#ticTopic').val(),
								'ticket_msg' : $('#ticMsg').val()
							},
							success: function(callback) {
								if (callback == '200') {
									$('#create_new_ticket').modal('hide');
									loadTickets();
								} else {
									$('#create_ticket_error_msg').html("Unknown error occur - " + callback);
									$('#create_ticket_error').slideDown(300);
									setTimeout(function() {
										$('#create_ticket_error').slideUp(300);
									}, 5000);
								}
							}
						});
						
					}
					
				});
				
				var open_ticket_id;
				$(document).on('click', '.theticket', function() {
					$('#comment_msg').val("");
					var ext_id = (this.id).split("-"); 
					var tid = ext_id[1];
					
					$.ajax({
						type: 'post',
						url: 'contactus_client.php',
						data: {
							'action' : 'loadoneticket',
							'ticket_id' : tid
						},
						success: function(callback) {var parsedCt = $.parseJSON(callback);
							$.each(parsedCt, function(key, val) {
								$('#ticket_title').html(val.ticket_topic);
								$('#tk_msg').html(val.ticket_message);
								
								open_ticket_id = val.ticket_id;
								loadComments(val.ticket_id);
								
								if (val.ticket_status == "open") {
									$('#tk_status').html("<span style=\"color: #008000; font-weight: bold\">Open</span>");
									$('#tk_action').html("<button class=\"btn btn-danger btn-xs\" id=\"tk_close\">Close</button>");
									$('#comment_msg').attr('disabled', false);
									$('#send_comment').attr('disabled', false);
									$('#comment_status').html("");
								} else {
									$('#tk_status').html("<span style=\"color: #FF0000; font-weight: bold\">Close</span>");
									$('#tk_action').html("<button class=\"btn btn-info btn-xs\" id=\"tk_open\">Reopen</button>");
									$('#comment_msg').attr('disabled', true);
									$('#send_comment').attr('disabled', true);
									$('#comment_status').html(" <em>(Disabled)</em>");
								}
								
								//console.log(val.ticket_added);
								var extd = (val.ticket_added).split(" ");
								var dates = extd[0].split("-"); 
								var times = extd[1].split(":");
								
								var d = new Date();
								d.setDate(dates[2]);
								d.setMonth(dates[1]-1);
								d.setFullYear(dates[0]);
								d.setHours(times[0]);
								d.setMinutes(times[1]);
								$('#tk_date').html(d.toDateString() + ", " + d.getHours() + ":" + d.getMinutes());
								
								$.ajax({
									async: false,
									url: "contactus_client.php?request=userfullname",
									success: function(dt) {
										$('#tk_sender_name').html(dt + ":");
									}
								});
								$.ajax({
									async: false,
									url: "getavt.php?uid=" + val.By_User_ID,
									success: function(ddtt) {
										$('#tk_start_avt').html(ddtt);
									}
								});
							});
						}
					});
					
					$('#view_ticket').modal('show');
					
					return false;
				});
				
				$(document).on('click', '#send_comment', function() {
					$('#loader').html(" <img src=\"img/loading.gif\" width=\"16\" height=\"16\" border=\"0\" />");
					if (!$('#comment_msg').val()) {
						alert("Please enter a comment message");
					} else {
						$.ajax({
							type: 'post',
							url: 'contactus_client.php',
							data: {
								'action' : "addnewcomment",
								'ticket_id' : open_ticket_id,
								'cmsg' : $('#comment_msg').val()
							},
							success: function(callback) {
								if (callback == '200') {
									//console.log($('#comment_msg').val());
									$('#comment_msg').val("");
									loadComments(open_ticket_id);
								} else {
									console.log(callback);
								}
							}
						})
					}
					$('#loader').html("");
				});
				
				$(document).on('click', '#tk_close', function() {
					$.ajax({
						type: 'post',
						url: 'contactus_client.php',
						data: {
							'action' : 'closeticket',
							'ticket_id' : open_ticket_id
						},
						success: function(callback) {
							if (callback == '200') {
								$('#tk_status').html("<span style=\"color: #FF0000; font-weight: bold\">Close</span>");
								$('#tk_action').html("<button class=\"btn btn-info btn-xs\" id=\"tk_open\">Reopen</button>");
								$('#comment_msg').attr('disabled', true);
								$('#send_comment').attr('disabled', true);
								$('#comment_status').html(" <em>(Disabled)</em>");
								loadTickets();
							} else {
								alert('Some error occurred, please report this problem to system administrator.');
								console.log(callback);
							}
						}
					});
				});
				
				$(document).on('click', '#tk_open', function() {
					$.ajax({
						type: 'post',
						url: 'contactus_client.php',
						data: {
							'action' : 'openticket',
							'ticket_id' : open_ticket_id
						},
						success: function(callback) {
							if (callback == '200') {
								$('#tk_status').html("<span style=\"color: #008000; font-weight: bold\">Open</span>");
								$('#tk_action').html("<button class=\"btn btn-danger btn-xs\" id=\"tk_close\">Close</button>");
								$('#comment_msg').attr('disabled', false);
								$('#send_comment').attr('disabled', false);
								$('#comment_status').html("");
								loadTickets();
							} else {
								alert('Some error occurred, please report this problem to system administrator.');
								console.log(callback);
							}
						}
					});
				});
				
			});
		</script>
	</body>
</html>
<?php
require_once "login_session_validator.php";
require_once "lib/nusoap.php";

//$client = new nusoap_client("http://localhost:8888/reward/rule_server.php",false);
include "connection/serverPath.php";
$client = new nusoap_client($serverPath . "rule_server.php",false);

$error = $client->getError();
if($error){
	echo "<h2>Constructor error</h2><pre>".$error."</pre>";
}

$startAt = $_POST['startAt'];

if($_POST['getRuleLog'] == true)
{
	
	$numberOfResult = $_POST['numberOfResult'];
	echo $client->call("getRuleLog",array("startAt"=>$startAt, "numberOfResult"=>$numberOfResult));
	exit();
}

if($_POST['hasNextPage'] == true)
{
	echo $client->call("hasNextPage",array("startAt"=>$startAt));
	exit();
}

$rule_id = $_GET['rule_id'];
$rule_name = $_GET['rule_name'];
$point = $_GET['point'];
$duration = $_GET['duration'];
$adminID = $_GET['adminID'];

if(isset($_GET['addRule'])){

	$client->call("addRule",array("ruleName"=>$rule_name,"point"=>$point,"duration"=>$duration,"adminID"=>$adminID));
}

if(isset($_GET['setRule'])){
	$client->call("setRule",array("ruleID"=>$rule_id,"ruleName"=>$rule_name,"point"=>$point,"duration"=>$duration,"adminID"=>$adminID));
}

if(isset($_GET['delRule'])){
	$client->call("deleteRule",array("ruleID"=>$rule_id,"adminID"=>$adminID));
}

$result = $client->call("displayRules");
$error2 = $client->getError();

if($error2){
	echo "<h2>Error</h2><pre>".$error2."</pre>";
}
else{
	echo $result;
}



?>
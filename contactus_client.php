<?php
session_start();
require_once "login_session_validator.php";
require_once "lib/nusoap.php";

//$client = new nusoap_client("http://localhost:8888/reward/rule_server.php",false);
include "connection/serverPath.php";
$client = new nusoap_client($serverPath . "contactus_server.php",false);

$error = $client->getError();
if($error){
	echo "<h2>Constructor error</h2><pre>".$error."</pre>";
}

if (isset($_GET['request'])) {
	if ($_GET['request'] == "userfullname") {
		echo $client->call("getUserFullName", array(
			"uid"	=>	$_SESSION['userid']
		));
		exit;
	} else {
		echo $client->call("getUserFullName", array(
			"uid"	=>	$_GET['request']
		));
		exit;
	}
}

if (!isset($_POST['action'])) {
	echo "Access Denied";
	exit;
}

if ($_POST['action'] == "loadticket") {
	
	$tickets = "";
	
	if ($_SESSION['permissionLevel'] == 'S') {
		$tickets = $client->call("getTotalTicket", array(
			"uid"	=>	$_SESSION['userid']
		));
	} else if ($_SESSION['permissionLevel'] == 'I' || $_SESSION['permissionLevel'] == 'A') {
		$tickets = $client->call("getTotalHighTicket", array(
			"uid"	=>	$_SESSION['userid']
		));
	}
	
	if ($tickets == 0) {
		echo "<center>You don't have any ticket</center>";
		exit;
	}
	
	if ($_SESSION['permissionLevel'] == 'S') {
		$tickets = $client->call("getTicket", array(
			"uid"	=>	$_SESSION['userid']
		));
	} else if ($_SESSION['permissionLevel'] == 'I' || $_SESSION['permissionLevel'] == 'A') {
		$tickets = $client->call("getHighUserTicket", array(
			"uid"	=>	$_SESSION['userid']
		));
	}
	
	echo $tickets;
}

if ($_POST['action'] == "loadcomments") {
	
	if (!isset($_POST['ticket_id'])) {
		echo "Access Denied";
		exit;
	}
	
	echo $listtickets = $client->call("getComments", array(
		"tid"	=>	$_POST['ticket_id']
	));
}

if ($_POST['action'] == "loadoneticket") {
	$ticket = $client->call("getOneTicket", array(
		"uid"	=>	$_SESSION['userid'],
		"tid"	=>	$_POST['ticket_id'],
		"perm"	=>	$_SESSION['permissionLevel']
	));
	
	echo $ticket;
}

if ($_POST['action'] == "loadhighusers") {
	$highusers = $client->call("getHighUsers");
	echo $highusers;
}

if ($_POST['action'] == "addnewticket") {
	if (!isset($_POST['receiver']) || !isset($_POST['ticket_topic']) || !isset($_POST['ticket_msg'])) {
		echo "Access Denied";
		exit;
	}
	
	$from = $_SESSION['userid'];
	$to = $_POST['receiver'];
	$topic = mysql_escape_string(stripcslashes($_POST['ticket_topic']));
	$msg = mysql_escape_string(nl2br(stripcslashes($_POST['ticket_msg'])));
	
	echo $client->call("addNewTicket", array(
		"from_id"	=>	$from,
		"to_id"		=> $to,
		"topic"		=> $topic,
		"msg"		=> $msg
	));
}

if ($_POST['action'] == "addnewcomment") {
			
	if (!isset($_POST['ticket_id']) || !isset($_POST['cmsg'])) {
		echo "Access Denied";
		exit;
	}	

	$inp_tid = $_POST['ticket_id'];
	$inp_uid = $_SESSION['userid'];
	$inp_msg = mysql_escape_string(nl2br(stripcslashes($_POST['cmsg'])));
	
	echo $client->call("addNewComment", array(
		"tid"	=>	$inp_tid,
		"uid"	=>	$inp_uid,
		"msg"	=>	$inp_msg
	));	
}

if ($_POST['action'] == "closeticket") {
			
	if (!isset($_POST['ticket_id'])) {
		echo "Access Denied";
		exit;
	}
	
	echo $client->call("closeTicket", array(
		"tid"	=>	$_POST['ticket_id']
	));	
}

if ($_POST['action'] == "openticket") {
			
	if (!isset($_POST['ticket_id'])) {
		echo "Access Denied";
		exit;
	}
	
	echo $client->call("openTicket", array(
		"tid"	=>	$_POST['ticket_id']
	));	
}

$error2 = $client->getError();

if($error2){
	echo "<h2>Error</h2><pre>".$error2."</pre>";
	exit;
}



?>
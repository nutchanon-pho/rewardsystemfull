<?
session_start();
include "login_session_validator.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Reward System</title>

	<!-- Bootstrap -->
	<link href="css/fontface.css" rel="stylesheet">
	<link href="css/bootstrap.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">
	<script src="jquery-1.11.0.min.js"></script>
	<script>
	function showResult(){
		var xmlhttp=new XMLHttpRequest();
		var getRules = true;	
		xmlhttp.onreadystatechange=function()
		{
			if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("result").innerHTML = xmlhttp.responseText;
				var obj = eval ("(" + xmlhttp.responseText + ")");
				var innerHTML = "<div class=\"table-responsive\"><table class=\"table table-hover\">";
				innerHTML += "<thead><tr>";
				innerHTML += "<th>Rule_ID</th>";
				innerHTML += "<th>Rule_Name</th>";
				innerHTML += "<th>Point</th>";
				innerHTML += "<th>Duration</th>";
				innerHTML += "</tr></thead><tbody>";
				for (var key in obj)
				{
					if (obj.hasOwnProperty(key))
					{
						innerHTML += "<tr id=\"" + obj[key].Rule_ID + "\">";
						innerHTML += "<td>" + obj[key].Rule_ID + "</td>";
						innerHTML += "<td>" + obj[key].Rule_Name + "</td>";
						innerHTML += "<td>" + obj[key].Point + "</td>";
						innerHTML += "<td>" + obj[key].Duration + "</td></tr>";
					}
				}
				innerHTML += "</tbody></table></div>";
				document.getElementById("result").innerHTML = innerHTML;
			}
		}
		xmlhttp.open("GET","rule_client.php?getRules="+getRules,true);

		xmlhttp.send();

	}
	</script>
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body onload = "showResult();">


		<?php
		$page = "rules"; 
		include "nav.php"; ?>

		<div class="container">
			<div class="row">
				
				<div class="col-sm-12 col-md-12 col-lg-12">
					<div class="box">
						<h1> Rules</h1>

						<hr />

						<div class="tab-content">
							
							
							
							<div class="tab-pane active" id="show_rule" >
								<div class="panel panel-default">
									<!-- Default panel contents -->
									<div class="panel-heading">
										<h4>&nbsp;&nbsp;<span class="glyphicon glyphicon-list-alt"> </span> Rules</h4>
									</div>

									<!-- Table -->
									<div id="result"></div>		

								</div>
								
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>

	</div>

</div>

<div class="container box" id="footer">
	&copy; Copyright 2014 <em>Reward System</em>. All Rights Reserved.
</div>
<a href="#" id="scroll_top">Scroll</a>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
<script src="js/scroll.js"></script>
<script src="js/avatarupload.js"></script>
</body>
</html>
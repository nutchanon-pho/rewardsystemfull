<?php
session_start();
include "login_session_validator.php";
if($_SESSION['authentication'] != true) //Not login yet
{
	$_SESSION['errorMsg'] = "empty";
	header("location: login.php");
	exit();
}
if($_SESSION['permissionLevel'] == 'I')
{
	header("location: instructor.php");
	exit();
}
else if($_SESSION['permissionLevel'] == 'A')
{
	header("location: admin.php");
	exit();
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Reward System</title>

	<!-- Bootstrap -->
	<link href="css/fontface.css" rel="stylesheet">
	<link href="css/bootstrap.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
		<script>
		$(document).ready(function(){
			var param = {};
			param['getRewardHistory'] = true;
			param['userID'] = $("#userID").html();
			$("#rewardHistory").ready(function(){
				$.post("userprofile_client.php",param,
					function(data,status){
						var json = $.parseJSON(data);
						var innerHTML = "<div class=\"table-responsive\"><table class=\"table table-hover\">";
						innerHTML += "<thead><tr class=\"success\">";
						innerHTML += "<th>Rule Name</th>";
						innerHTML += "<th>Point</th>";
						innerHTML += "<th>Instructor Name</th>"
						innerHTML += "<th>Given on</th>";
						innerHTML += "</tr></thead><tbody>";
						$(json).each(function(i,obj){
							innerHTML += "<tr><td>" + obj.Rule_Name + "</td>";
							innerHTML += "<td>" + obj.Point + "</td>";
							innerHTML += "<td>" + obj.Instructor_Name + "</td>";
							innerHTML += "<td>" + obj.Timestamp + "</td></tr>";
						});
						innerHTML += "</tbody></table></div>";
						$("#rewardHistoryModalBody").html(innerHTML);
					});
			});
			$(".editProfileForm").hide();
			$("#editProfile").click(function(){
				$(".editProfileForm").show();
				$(".profileInfo").hide();
			});
			$("#saveChanges").click(function(){
				$(".editProfileForm").hide();
				$(".profileInfo").show();
				var param = {};
				param['editProfile'] = true;
				param['userID'] = $("#userID").html();;
				param['year'] = $("#editYear option:selected").text();
				param['track'] = $("#editTrack option:selected").text();
				param['email'] = $("#editEmail").val();
				param['tel'] = $("#editTel").val();
				$.post("userprofile_client.php",param,function(data, status){
					location.reload();
				});
			});

		});
</script>
</head>
<body>

	<?php 
	$page = "profile";
	include "nav.php";
	include "avtupload_popup.php";
	require_once "lib/nusoap.php";
		

		include "connection/serverPath.php";
		$client = new nusoap_client($serverPath . "userprofile_server.php",false);
	    //$client = new nusoap_client("http://localhost:8888/reward/userprofile_server.php",false);

		if(isset($_SESSION['authentication']) && $_SESSION['authentication'] == true)//Valid user
		{
			$userid = $_SESSION['userid'];
			$permissionLevel = $_SESSION['permissionLevel'];
			$result = $client->call("getUserInfo",array("uid"=>$userid));
		}
		if($permissionLevel=='S') //Set score for student
		{
			$s_score = $client->call("getStudentScore",array("std"=>$userid));
			$b_score = $client->call("getBannScore",array("bann"=>$result['b_id']));
		}
		else //not student
		{
			$s_score = 0;
			$b_score = 0;
		}
		?>

		<div class="container">
			<div class="row">

				<div class="col-sm-12 col-md-3 col-lg-3">
					<div class="box">
						<div class="row">
							<div class="col-xs-4 col-sm-4 col-md-4 col-lg-12">
								<center id="user_avt">
									<img src="img/loading.gif" width="32" height="32" border="0" />
								</center>
							</div>
							<div class="col-xs-8 col-sm-8 col-md-8 col-lg-12">
								<span class="visible-lg"><br /></span>
								<p class="lead"><strong><?echo $result["name"]." ".$result["surname"];?></strong></p>
								<p><span class="label label-primary"><?php echo $result["id"];?></span> <span class="label label-success"><?php echo $result['year']; ?> Year</span> <span class="label label-warning">Baan <?php echo $result['b_id']; ?></span></p>
							</div>
						</div>
						<br />
						<div>
							<form action="login.php" method="POST">
								<input type="hidden" name="signout" value="true">
								<center><button type="submit" class="btn btn-xs btn-danger">Sign Out</button></center>
							</form>
						</div>
					</div>
				</div>


				<div class="col-sm-12 col-md-9 col-lg-9">

					<div class="box">
						<h1><span class="glyphicon glyphicon-user"> </span> 
							Student / <?echo $result["name"]." ".$result["surname"];?> <!-- Single button -->
							<div class="btn-group">
								<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
									Edit <span class="caret"></span>
								</button>
								<ul class="dropdown-menu dropdown-normal" role="menu">
									<li>
										<a id="editProfile">Profile</a>
									</li>
									<li>
										<a href="#" data-toggle="modal" data-target="#avatar_upload_modal">Avatar</a>
									</li>
								</ul>
							</div></h1>
							<hr />
							<div class="row">
								<div class="col-sm-12 col-md-6 col-lg-6">
									<div class="panel panel-primary" style="text-align: center; cursor:pointer;" data-toggle="modal" data-target="#rewardHistoryModal">
										<div class="panel-heading">
											<h4>Student's Score</h4>
										</div>
										<div class="panel-body">
											<?echo $s_score;?> points
										</div>
									</div>
								</div>
								<!-- Modal -->
								<div class="modal fade" id="rewardHistoryModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
												<h2 class="modal-title" id="myModalLabel"> <span class="glyphicon glyphicon-star"></span> Reward History</h2>
											</div>
											<div class="modal-body" id="rewardHistoryModalBody">
												...
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
											</div>
										</div>
									</div>
								</div>
								<div class="col-sm-12 col-md-6 col-lg-6">
									<div class="panel panel-danger" style="text-align: center;">
										<div class="panel-heading">
											<h4>Baan's Score</h4>
										</div>
										<div class="panel-body">
											<?echo $b_score; ?> points
										</div>
									</div>
								</div>
							</div>
							<div class="table-responsive">
							<table class="table table-bordered">
								<tr>
									<td colspan="2" class="td-panel"><h4>&nbsp;&nbsp;Profile</h4></td>
								</tr>
								<tr>
									<td style="width: 15%;">Student</td>
									<td id="userID"><?php echo $result["id"];?></td>
								</tr>
								<tr>
									<td style='width: 15%;'>Baan:</td>
									<td>
										<?php echo $result['bann'];?>
									</td>
								</tr>
								<tr>
									<td style='width: 15%;'>Year:</td>
									<td>
										<span class="profileInfo"><?php echo $result['year'];?></span>
										<select class="editProfileForm" id="editYear">
											<option value="1st" <?if($result['year']=="1st"){echo "selected=\"selected\"";}?>>1st</option>
											<option value="2nd" <?if($result['year']=="2nd"){echo "selected=\"selected\"";}?>>2nd</option>
											<option value="3rd" <?if($result['year']=="3rd"){echo "selected=\"selected\"";}?>>3rd</option>
											<option value="4th" <?if($result['year']=="4th"){echo "selected=\"selected\"";}?>>4th</option>
										</select>

									</td>
								</tr>
								<tr>
									<td style='width: 15%;'>Track:</td>
									<td>
										<span class="profileInfo"><?php echo $result['track'];?></span>
										<select class="editProfileForm" id="editTrack">
											<option value="Multimedia"
												<?if($result['track']=="Multimedia"){echo "selected=\"selected\"";}?>
												>Multimedia</option>
											<option value="Computer Science" 
												<?if($result['track']=="Computer Science"){echo "selected=\"selected\"";}?>
												>Computer Science</option>
											<option value="Database" <?if($result['track']=="Database"){echo "selected=\"selected\"";}?>>Database</option>
											<option value="Computer Network" <?if($result['track']=="Computer Network"){echo "selected=\"selected\"";}?>>Computer Network</option>
											<option value="Management Information System" <?if($result['track']=="Management Information System"){echo "selected=\"selected\"";}?>>Management Information System</option>
											<option value="Health Information Technology" <?if($result['track']=="Health Information Technology"){echo "selected=\"selected\"";}?>>Health Information Technology</option>
											<option value="Software Engineering" <?if($result['track']=="Software Engineering"){echo "selected=\"selected\"";}?>>Software Engineering</option>
											<option value="E-business" <?if($result['track']=="E-business"){echo "selected=\"selected\"";}?>>E-business</option>
										</select>
									</td>
								</tr>
								<tr>
									<td style="width: 15%;">Email:</td>
									<td>
										<span class="profileInfo"><?php echo $result["email"];?></span>
										<div class="input-group editProfileForm" >
											<input type="text" class="form-control" id="editEmail" placeholder="Email" value="<?php echo $result["email"];?>">
										</div>
									</td>
								</tr>
								<tr>
									<td style="width: 15%;">Tel:</td>
									<td>
										<span class="profileInfo"><?php echo $result["tel"];?></span>
										<div class="input-group editProfileForm" >
											<input type="text" class="form-control" id="editTel" placeholder="Telephone" value="<?php echo $result["tel"];?>">
										</div>
									</td>
								</tr>
							</table>
							</div>
							<form action="login.php" method="POST" style="display:inline;">
								<input type="hidden" name="signout" value="true">
								<button type="submit" class="btn btn-danger">Sign Out</button>
							</form>
							<button type="button" class="btn btn-primary editProfileForm" id="saveChanges">Save changes</button>
						</div>
					</div>
				</div>
			</div>

			<div class="container box" id="footer">
				&copy; Copyright 2014 <em>Reward System</em>. All Rights Reserved.
			</div>
			<a href="#" id="scroll_top">Scroll</a>


			<!-- Include all compiled plugins (below), or include individual files as needed -->
			<script src="js/bootstrap.min.js"></script>
			<script src="js/jqueryform.js"></script>
			<script src="js/scroll.js"></script>
			<script src="js/avatarupload.js"></script>
		</body>
		</html>
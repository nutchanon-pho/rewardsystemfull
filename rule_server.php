<?php
require_once "lib/nusoap.php";

function createConnection(){
	// Create connection
	include "connection/databaseConnection.php";
	$con=mysqli_connect($host,$username,$password,$db_name);
	//$con=mysqli_connect("localhost","root","root","RewardDatabase");
	// Check connection
	if (mysqli_connect_errno()){
		return "Failed to connect to MySQL: " . mysqli_connect_error();
	}
	return $con;
}

function closeConnection($con){
	mysqli_close($con);
}

function recordLog($ruleID, $adminID, $action,$newRuleID)
{
	$con = createConnection();
	$logquery = "INSERT INTO  `RulesLog` (  `Transaction_ID` ,  `Rule_ID` ,  `Admin_ID` ,  `transactionTimestamp` ,  `Action`,`newRuleID` ) 
	VALUES (
		NULL ,  '".$ruleID."',  '".$adminID."', NOW( ) ,  '".$action."',".$newRuleID."
		)";
	mysqli_query($con,$logquery);
	closeConnection($con);
}

function getRecentRuleID(){
	$con = createConnection();
	$query = "UPDATE `Rules` SET `Active`= 0 WHERE now() > Duration"; //Duration Checking
	$result = mysqli_query($con,$query);
	
	$query = "SELECT Rule_ID,Rule_Name,Point FROM Rules ORDER BY Rule_id DESC LIMIT 1";
	$result = mysqli_query($con,$query);

	$ruleID = 0;
	while($row = mysqli_fetch_array($result)){
		$ruleID = $row['Rule_ID'];
	}	  	
	closeConnection($con);
	return $ruleID;
}

function displayRules(){
	$con = createConnection();
	$query = "SELECT * FROM Rules WHERE Active = 1 ORDER BY Rule_ID";
	$result = mysqli_query($con,$query);
	closeConnection($con);
	$json = parseRule($result);		// Comment th width=\"150\"is line to disable displayRule
	return $json;								// Then, change th width=\"150\"e returned value to $result
}

function addRule($ruleName,$point,$duration, $adminID){
	$con = createConnection();
	$query="INSERT INTO Rules (Rule_Name, Point, Duration, Active)
	VALUES ('".$ruleName."',".$point.",\"".$duration."\", 1 )";
	$result = mysqli_query($con,$query);
	$ruleID = getRecentRuleID(); 	
	recordLog($ruleID, $adminID, "Add",$ruleID);
	closeConnection($con);
}

function setRule($ruleID,$ruleName,$point,$duration,$adminID)
{
	addRule($ruleName,$point,$duration,$adminID);
	//Delete add log record
	$con = createConnection();
	$query= "UPDATE Rules SET Duration = ".$duration." ,Active = 0 WHERE Rule_ID = ".$ruleID;
	$newRuleID = getRecentRuleID();
	$deleteQuery = "DELETE FROM RulesLog WHERE Rule_ID =".$newRuleID;
	mysqli_query($con,$deleteQuery);
	$result = mysqli_query($con,$query);
	closeConnection($con);
	recordLog($ruleID, $adminID, "Edit",$newRuleID);
}

function deleteRule($ruleID,$adminID){	
	$con = createConnection();
	recordLog($ruleID, $adminID, "Delete", 0);
	$query= "UPDATE Rules SET Active = 0 WHERE Rule_ID = ".$ruleID;
	$result = mysqli_query($con,$query);
	closeConnection($con);
}

function getRuleLog($startAt, $numberOfResult)
{
	$con = createConnection();
	$query = "SELECT transactionTimestamp as \"Timestamp\",
		r1.Rule_ID as \"OLD RULE ID\", 
		r1.Rule_Name as \"Old_Rule_Name\", 
		r1.Point as \"Old_Point\",
		r2.Rule_ID as \"NEW RULE ID\",
		r2.Rule_Name as \"New_Rule_Name\",
		r2.Point as \"New_Point\" ,
		Admin_ID,
		User_Name,
		Action
		FROM RulesLog rul
		LEFT JOIN Rules r1 ON rul.Rule_ID = r1.Rule_ID
		LEFT JOIN Rules r2 ON rul.newRuleID = r2.Rule_ID
		LEFT JOIN Users u on rul.Admin_ID = u.User_ID
		ORDER BY Timestamp DESC
		LIMIT ". $startAt ." , ". $numberOfResult;
	$result = mysqli_query($con, $query);
	closeConnection($con);
	return parseRuleLog($result);
}

function hasNextPage($startAt)
{
	$con = createConnection();
	$count_query = "SELECT COUNT(*) AS \"NumberOfRecord\" FROM RulesLog";
	$count_result = mysqli_query($con, $count_query);
	closeConnection($con);
	$row = mysqli_fetch_array($count_result);
	$numberOfRecord = $row['NumberOfRecord'];
	if($startAt >= $numberOfRecord) //Page out of bound
		return -1;
	return 1;
}

function parseRuleLog($result){
	$jsonResult = "[";
	$i = 0;
	while($row = mysqli_fetch_array($result))
	{
		if($i != 0)
			$jsonResult .= ",";
		$jsonResult .= "{";
		$jsonResult .= ' "Timestamp" : "' . $row['Timestamp'] . '", ';
		$jsonResult .= ' "Old_Rule_Name" : "' . $row['Old_Rule_Name'] . '", ';
		$jsonResult .= ' "Old_Point" : "' . $row['Old_Point'] . '", ';
		$jsonResult .= ' "New_Rule_Name" : "' . $row['New_Rule_Name'] . '", ';
		$jsonResult .= ' "New_Point" : "' . $row['New_Point'] . '", ';
		$jsonResult .= ' "User_Name" : "' . $row['User_Name'] . '", ';
		$jsonResult .= ' "Action" : "' . $row['Action'] . '"';
		$jsonResult .= "}";
		$i++;
	}
	$jsonResult .= "]";
	return $jsonResult;
}

function parseRule($result){
	$jsonResult = "[";
	$i = 0;
	while($row = mysqli_fetch_array($result))
	{
		if($i != 0)
			$jsonResult .= ",";
		$jsonResult .= "{";
		$jsonResult .= ' "Rule_ID" : "' . $row['Rule_ID'] . '", ';
		$jsonResult .= ' "Rule_Name" : "' . $row['Rule_Name'] . '", ';
		$jsonResult .= ' "Point" : "' . $row['Point'] . '", ';
		$jsonResult .= ' "Duration" : "' . $row['Duration'] . '" ';
		$jsonResult .= "}";
		$i++;
	}
	$jsonResult .= "]";
	return $jsonResult;
}





// create object to deal with width=\"150\" service provider
$server = new soap_server();
$server->register("displayRules");
$server->register("addRule");
$server->register("setRule");
$server->register("deleteRule");
$server->register("recordLog");
$server->register("getRuleLog");
$server->register("hasNextPage");

if(!isset($HTTP_RAW_POST_DATA))
	$HTTP_RAW_POST_DATA = file_get_contents('php://input');

$server->service($HTTP_RAW_POST_DATA);

?>
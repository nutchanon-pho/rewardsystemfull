<?php
session_start();
if (!isset($_SESSION['userid'])) {
	echo "Access Denied";
	exit;
}
if ($_FILES["avt_file"]["error"] == 4) {
	echo "Please select an image to upload.";
	exit;	
}
$allowedExts = array("gif", "jpeg", "jpg", "png", "JPG");
$temp = explode(".", $_FILES["avt_file"]["name"]);
$extension = end($temp);

if ((($_FILES["avt_file"]["type"] == "image/gif")
|| ($_FILES["avt_file"]["type"] == "image/jpeg")
|| ($_FILES["avt_file"]["type"] == "image/jpg")
|| ($_FILES["avt_file"]["type"] == "image/pjpeg")
|| ($_FILES["avt_file"]["type"] == "image/x-png")
|| ($_FILES["avt_file"]["type"] == "image/png"))
&& ($_FILES["avt_file"]["size"] <= 800000) // Limit file size 800 KB
&& in_array($extension, $allowedExts)) {
	
  if ($_FILES["avt_file"]["error"] > 0) {
  	
    echo "Unknown error occur: " . $_FILES["avt_file"]["error"];
	  
  } else {
  	
	if ($_FILES["avt_file"]["size"] > 800000) {
		echo "Your avatar file is too large (Bigger than 800 KB), please select another avatar.";
		exit;
	}
    
    //$fname = $_FILES["file"]["name"];
    $fname = md5($_SESSION['userid']) . "." . $extension;
	
    echo "200";
    
  }
  
} else {
	
	echo "Invalid file type. Only image file types are allowed (jpg, jpeg, png, gif)";
  
}
?>
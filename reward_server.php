<?php

require_once "lib/nusoap.php";

function createConnection(){
	// Create connection
	include "connection/databaseConnection.php";
	$con=mysqli_connect($host,$username,$password,$db_name);
	//$con=mysqli_connect("localhost","root","root","RewardDatabase");
	// Check connection
	if (mysqli_connect_errno()){
		return "Failed to connect to MySQL: " . mysqli_connect_error();
	}
	return $con;
}

function closeConnection($con){
	mysqli_close($con);
}

///////// save transaction into reward table///////////////
function giveReward($instructorId,$studentId,$ruleId){
	
	$con = createConnection();
	
	$date = date('Y-m-d H:i:s');
	
	$query="INSERT INTO Rewards (Student_ID, Instructor_ID, Rule_ID, Time_Stamp)
				VALUES (".$studentId.",".$instructorId.",".$ruleId.",now())";
	 
				 
	$result = mysqli_query($con,$query);
	
	closeConnection($con);
	return "The reward is given.";
}

////////// search ////////////////////////////////
function searchInfo($searchBy,$keyword){
	
	$con = createConnection();
	
	switch ($searchBy) {
    case "studentID":
		$query= "SELECT Users.User_ID,Users.User_Name,Users.User_Surname,Students.Bann_ID
		FROM Students,Users 
		WHERE Students.Student_ID =".$keyword." 
		AND Students.Student_ID = Users.User_ID";
			
		$result = mysqli_query($con,$query);
		closeConnection($con);
		$table = showResult($result);		// Comment this line to disable displayRule
		return $table;
        break;
    case "name":
        $query= "SELECT Users.User_ID,Users.User_Name,Users.User_Surname,Students.Bann_ID
		FROM Students,Users
		WHERE Users.User_Name LIKE \"%".$keyword."%\" AND Students.Student_ID = Users.User_ID";	
		$result = mysqli_query($con,$query);
		closeConnection($con);
		$table = showResult($result);		// Comment this line to disable displayRule
		return $table;
        break;
    case "surname":
		$query= "SELECT Users.User_ID,Users.User_Name,Users.User_Surname,Students.Bann_ID
		FROM Students,Users
		WHERE Users.User_Surname LIKE \"%".$keyword."%\" AND Students.Student_ID = Users.User_ID";
		$result = mysqli_query($con,$query);
		closeConnection($con);
		$table = showResult($result);		// Comment this line to disable displayRule
		return $table;
        break;
	case "bann":
		$query= "SELECT Users.User_ID,Users.User_Name,Users.User_Surname,Students.Bann_ID
		FROM Students,Users
		WHERE Students.Bann_ID =".$keyword." AND Students.Student_ID = Users.User_ID";
		$result = mysqli_query($con,$query);
		closeConnection($con);
		$table = showResult($result);		// Comment this line to disable displayRule
		return $table;
        break;
	}
	
	
	
	
}

function showResult($result){
		
		$table =//"<form action='giveReward.php' method='post'>
		"<div class=\"table-responsive\"><table class=\"table \">
		<thead>
			<tr>
				<th>Student_ID</th>
				<th>Name</th>
				<th>Surname</th>
				<th>Bann ID</th>
				<th>Action</th>
			</tr>
		</thead>";
		$table .= "<tbody>";
		$row = mysqli_fetch_array($result);
		if(!isset($row)){
			return "<tr><td colspan=\"5\"><p><center>No such Record</center></p></td></tr>";
		}
		do
		  {
			  
			  $table .= "<tr>";
			  $table .= "	<td>" . $row['User_ID'] . "</td>";
			  $table .= "	<td>" . $row['User_Name'] . "</td>";
			  $table .= "	<td>" . $row['User_Surname'] . "</td>";
			  $table .= "	<td>" . $row['Bann_ID'] . "</td>";
			  $table .= "	<td><button id=\"".$row['User_ID']."\"  onclick=\"buttonClick(this.id, '" . $row['User_Name'] . "', '" . $row['User_Surname'] . "')\" class=\"btn btn-primary\" data-toggle=\"modal\" data-target=\".bs-example-modal-lg\"><span class=\"glyphicon glyphicon-star\"></span> give reward</button></td>";
			  $table .= "</tr>";
		  }while($row = mysqli_fetch_array($result));
		$table .= "</tbody>";
		$table .= "</table></div>";
		
	
	
	return $table;
}



// create object to deal with service provider
$server = new soap_server();
$server->register("giveReward");
$server->register("searchInfo");


if(!isset($HTTP_RAW_POST_DATA))
	$HTTP_RAW_POST_DATA = file_get_contents('php://input');

$server->service($HTTP_RAW_POST_DATA);

?>
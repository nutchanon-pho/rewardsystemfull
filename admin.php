<?php 
session_start();
include "login_session_validator.php";
if($_SESSION['authentication'] != true || $_SESSION['permissionLevel'] != "A") //Not login yet
{
	$_SESSION['errorMsg'] = "empty";
	header("location: login.php");
	exit();
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Reward System</title>

	<!-- Bootstrap -->
	<link href="css/fontface.css" rel="stylesheet">
	<link href="css/bootstrap.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	<script>
			/*
			 * Rules
			 */
			 function displayJSON(result)
			 {
			 	var obj = eval ("(" + result + ")");
			 	var innerHTML = "<div class=\"table-responsive\"><table class=\"table table-hover\">";
			 	innerHTML += "<thead><tr>";
			 	innerHTML += "<th>Rule_ID</th>";
			 	innerHTML += "<th>Rule_Name</th>";
			 	innerHTML += "<th>Point</th>";
			 	innerHTML += "<th>Duration</th>";
			 	innerHTML += "<th>Option</th>";
			 	innerHTML += "</tr></thead><tbody>";
			 	for (var key in obj)
			 	{
			 		if (obj.hasOwnProperty(key))
			 		{
			 			innerHTML += "<tr id=\"" + obj[key].Rule_ID + "\">";
			 			innerHTML += "<td>" + obj[key].Rule_ID + "</td>";
			 			innerHTML += "<td>" + obj[key].Rule_Name + "</td>";
			 			innerHTML += "<td>" + obj[key].Point + "</td>";
			 			innerHTML += "<td>" + obj[key].Duration + "</td>";
			 			innerHTML += "<td>";
			 			innerHTML += "<button type=\"button\" class=\"btn btn-warning btn-sm\" id=\"edit\" onclick=\"editRule(" + 	obj[key].Rule_ID + ",'" + obj[key].Rule_Name +"','"+ obj[key].Point +"','"+ obj[key].Duration +"')\"><span class=\"glyphicon glyphicon-pencil\"></span> Edit</button>";
			 			innerHTML += "<button type=\"button\" class=\"btn btn-danger btn-sm\" onclick=\"confirmDelete(" + obj[key].Rule_ID + ")\"><span class=\"glyphicon glyphicon-remove\"></span> Delete</button></td>";
			 			innerHTML += "</tr><tr id = \"e" + obj[key].Rule_ID + "\"></tr>";
			 		}
			 	}
			 	innerHTML += "</tbody></table></div>";
			 	document.getElementById("result").innerHTML = innerHTML;
			 }
			 function confirmDelete(id)
			 {
			 	$("#alertBox").hide();
				$("#alertBox").show('slow');
				var fn = "deleteRule("+id+")";
				$("#deleteButton").attr("onclick",fn);
			 }
			 function showResult(){
			 	var xmlhttp=new XMLHttpRequest();
			 	var getRules = true;	
			 	xmlhttp.onreadystatechange=function()
			 	{
			 		if (xmlhttp.readyState==4 && xmlhttp.status==200)
			 		{
						//document.getElementById("result").innerHTML = xmlhttp.responseText;
						displayJSON(xmlhttp.responseText);
					}
				}
				xmlhttp.open("GET","rule_client.php?getRules="+getRules,true);
				xmlhttp.send();
				
			}

			function addRule(){
				//$("#addForm").hide("slow");		
				var xmlhttp=new XMLHttpRequest();
				var addRule = true;	
				var getRules = true;				
				var rule_name = document.getElementById("rule_name").value;
				var point = document.getElementById("point").value;
				var range = document.getElementById("range").value;
				var curDate = new Date();
				var endDate = calDuration(curDate,range);
				var adminID = document.getElementById("adminID").value;
				xmlhttp.onreadystatechange=function()
				{
					if (xmlhttp.readyState==4 && xmlhttp.status==200)
					{    			
						displayJSON(xmlhttp.responseText);
					}
				}

				xmlhttp.open("GET","rule_client.php?addRule="+addRule
					+"&point="+point
					+"&rule_name="+rule_name
					+"&duration="+endDate
					+"&adminID="+adminID,true);		
				xmlhttp.send();
			}

			function setRule(id,name,point,duration){
				$("#editForm").hide("slow");
				var xmlhttp=new XMLHttpRequest();
				var setRule = true;				
				var rule_name = document.getElementById("erule_name").value;
				var rule_id = id;
				var point = document.getElementById("epoint").value;
				var adminID = document.getElementById("adminID").value;
				xmlhttp.onreadystatechange=function()
				{
					if (xmlhttp.readyState==4 && xmlhttp.status==200)
					{    			
						displayJSON(xmlhttp.responseText);
					}
				}
				xmlhttp.open("GET","rule_client.php?setRule="+setRule
					+"&point="+point
					+"&rule_name="+rule_name
					+"&rule_id="+rule_id
					+"&duration="+duration
					+"&adminID="+adminID,true);		
				xmlhttp.send();
			}		

			function clearForm(){		
				document.getElementById("rule_name").value = "";
				document.getElementById("point").value = "";
				document.getElementById("range").value = "";
				//$("#addForm").hide("slow");
			}
			
			function clearEditForm(id){	
				$("#"+id).show();		
				document.getElementById("e"+id).innerHTML="";
			}

			function editRule(id,name,point,duration){	
				$("#"+id).hide();			
				$("#editForm").show("slow");
				//$("#addForm").hide("slow");
				var table = "<td>" +id+ "</td>";
				table += "<td><input class=\"form-control\" type=\"text\" id=\"erule_name\" value = \""+name+"\"/></td>";
				table += "<td><input class=\"form-control\" type=\"text\" id=\"epoint\" onkeyup=\"validateNumber(this.id, this.value)\" value = \""+point+"\"/></td>";
				table += "<td>"+ duration +"</td>";
				table += "<td><button class=\"btn btn-success btn-sm\" type=\"button\" onclick='setRule(\""+id+"\",\""+name+"\",\""+point+"\",\""+duration+"\")'><span class=\"glyphicon glyphicon-floppy-disk\"></span> Save</button>";
				table += "<button class=\"close\" type=\"button\" onclick=\"clearEditForm("+id+")\">&times;</button></td>";
				document.getElementById("e"+id).innerHTML=table;
			}

			function deleteRule(id)
			{	
					$("#"+id).hide("slow");	
					$("#editForm").hide("slow");
					//$("#addForm").hide("slow");	
					var xmlhttp=new XMLHttpRequest();
					var delRule = true;	
					var getRules = true;				
					var rule_id = id;
					var adminID = document.getElementById("adminID").value;
					
					xmlhttp.onreadystatechange=function()
					{
						if (xmlhttp.readyState==4 && xmlhttp.status==200)
						{
							displayJSON(xmlhttp.responseText);
						}
					}
					xmlhttp.open("GET","rule_client.php?delRule="+delRule+"&rule_id="+rule_id+"&adminID="+adminID,true);
					xmlhttp.send();
			}

			function calDuration(curDate,range){
				var duration = new Date(curDate.getTime()+(range*24*60*60*1000));
				var d = duration.getDate();
				if(d<10) d = "0"+d;
				var m = (duration.getMonth()+1);
				if(m<10) m = "0"+m;
				var y = duration.getFullYear();
				var endDate = y+m+d;
				return endDate;
			}

			function validateNumber(id, input)
			{
				var patt = /\D/g;
				if(input.match(patt) != null)
				{
					$("#" + id).val(input.substring(0,input.length-1));
				}
			}

			function hasNextPage(pageNumber)
			{
				var param = {};
				param['hasNextPage'] = true;
				param['startAt'] = (pageNumber - 1) * 10;
				var result = null;
				$.post("rule_client.php",param,
					function(data,status){
						if(data != 1) //No next page
						{
							$("#nextButton").hide();
						}
						else //Has next page
						{
							$("#nextButton").show();
						}
					});
			}

			function ruleLogAjax(pageNumber)
			{
				$("#nextButton").hide();
				$("#previousButton").hide();
				var param = {};
				param['getRuleLog'] = true;
				param['startAt'] = (pageNumber - 1) * 10;
				param['numberOfResult'] = 10;
				$.post("rule_client.php",param,
					function(data,status){
						if(data)
						{
							var json = $.parseJSON(data);
							var innerHTML = "<div class=\"table-responsive\"><table class=\"table table-hover\">";
							innerHTML += "<thead><tr class=\"success\">";
							innerHTML += "<th>Timestamp</th>";
							innerHTML += "<th>Old Rule Name</th>";
							innerHTML += "<th>Old Point</th>"
							innerHTML += "<th>New Rule Name</th>";
							innerHTML += "<th>New Point</th>";
							innerHTML += "<th>Admin Name</th>";
							innerHTML += "<th>Action</th>";
							innerHTML += "</tr></thead><tbody>";
							$(json).each(function(i,obj){
								innerHTML += "<tr><td>" + obj.Timestamp + "</td>";
								if(obj.Action == "Add")
								{
									innerHTML += "<td> - </td>";
									innerHTML += "<td> - </td>";
								}
								else
								{
									innerHTML += "<td>" + obj.Old_Rule_Name + "</td>";
									innerHTML += "<td>" + obj.Old_Point + "</td>";
								}
								if(obj.Action == "Delete")
								{
									innerHTML += "<td> - </td>";
									innerHTML += "<td> - </td>";
								}
								else
								{
									innerHTML += "<td>" + obj.New_Rule_Name + "</td>";
									innerHTML += "<td>" + obj.New_Point + "</td>";
								}
								innerHTML += "<td>" + obj.User_Name + "</td>";
								innerHTML += "<td>" + obj.Action + "</td></tr>";
							});
							innerHTML += "</tbody></table></div>";
							$("#ruleLogTable").html(innerHTML);
							hasNextPage(pageNumber+1);
						}
						else
						{
							return false;
						}
					});
				}

			$(document).ready(function() {
				var pageNumber = 1;
				// Rules
				$('#myTab a').click(function (e) {
					e.preventDefault()
					$(this).tab('show')
				})
				$("#addForm").hide();
				$("#editForm").hide();

				$("#addrule").click(function() {
					$("#addForm").show("slow");
					 $('#add_rule_modal').modal();
					});
				$("#alertBox").hide();
				$("#deleteButton").click(function(){
					$("#alertBox").hide('slow');
				});
				$("#cancelButton").click(function(){
					$("#alertBox").hide('slow');
				});
				
				//Rule Logx
				$('a[href="#ruleLog"]').click(function()
				{
					ruleLogAjax(pageNumber);
					if(pageNumber==1)
					{
						$("#previousButton").hide();
					}
				});

				$("#previousButton").click(function(){
					ruleLogAjax(--pageNumber);
				});
				$("#nextButton").click(function(){
					ruleLogAjax(++pageNumber);
				});

				$(".navigationButton").click(function(){ //Show Previous Button Function
					$("#previousButton").hide();
					if(pageNumber!=1)
					{
						$("#previousButton").show();
					}
					
				});
});
</script>
<script src="js/bootstrap.min.js"></script>
<script src="js/scroll.js"></script>
<script src="js/jqueryform.js"></script>
<script src="js/avatarupload.js"></script>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body onload="showResult()">

		<?php
		$page = "profile";
		include "nav.php";
		include "avtupload_popup.php";
		require_once "lib/nusoap.php";

		include "connection/serverPath.php";
		$client = new nusoap_client($serverPath . "userprofile_server.php",false);
		//$client = new nusoap_client("http://localhost:8888/reward/userprofile_server.php",false);

		if(isset($_SESSION['authentication']) && $_SESSION['authentication'] == true)//Valid user
		{
			$userid = $_SESSION['userid'];
			$permissionLevel = $_SESSION['permissionLevel'];
			$result = $client->call("getUserInfo",array("uid"=>$userid));
		}
		?>

		<div class="container">
			<input type="hidden" id="adminID" value="<?echo $_SESSION['userid'];?>">
			<div class="row">

				<div class="col-sm-12 col-md-3 col-lg-3">
					<div class="box">
						<div class="row">
							<div class="col-xs-4 col-sm-4 col-md-4 col-lg-12">
								<center id="user_avt">
									<img src="img/loading.gif" width="32" height="32" border="0" />
								</center>
							</div>
							<div class="col-xs-8 col-sm-8 col-md-8 col-lg-12">
								<span class="visible-lg"><br /></span>
								<p class="lead"><strong><?echo $result["name"]." ".$result["surname"];?></strong></p>
								<p><span class="label label-primary">Administrator</span></p>
							</div>
						</div>
						<br />
						<div>
							<form action="login.php" method="post">
								<input type="hidden" name="signout" value="true">
								<center><button type="submit" class="btn btn-xs btn-danger">sign out</button></center>
							</form>
						</div>
					</div>
				</div>

				<div class="col-sm-12 col-md-9 col-lg-9">
					<div class="box">
						<h1>
							<span class="glyphicon glyphicon-user"> </span> Administrator / <?echo $result["name"]." ".$result["surname"];?> <small><button class="btn btn-xs" data-toggle="modal" data-target="#avatar_upload_modal">Change Avatar</a></button>
						</h1>
						<hr />
						<ul class="nav nav-tabs" id="myTab">
							<li class="active"><a href="#ruleManagement" data-toggle="tab">Rule Management</a></li>
							<li><a href="#ruleLog" data-toggle="tab">Rule Log</a></li>
						</ul>
						<div class="alert alert-danger fade in" id="alertBox">
					      <h3>Are you sure you want to delete this rule?</h3>
					        <button type="button" class="btn btn-danger" id="deleteButton" onclick="deleteRule()">Delete</button>
					        <button type="button" class="btn btn-default" id="cancelButton">Cancel</button>
					      </p>
					    </div>
						<div class="tab-content">
							<div class="tab-pane active" id="ruleManagement">
								<div class="panel panel-default">
									<!-- Default panel contents -->
									<div class="panel-heading">
										<h4>&nbsp;&nbsp;<span class="glyphicon glyphicon-list-alt"> </span> Rules&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											<button type="button" class="btn btn-success" id="addrule" data-toggle="modal" data-target="#add_rule_modal">
												<span class="glyphicon glyphicon-plus"></span> Add rule
											</button>
										</h4>
									</div> <!-- end panel heading -->
									<div id="result"></div>
									<div class="modal fade" id="add_rule_modal">
										<div class="modal-dialog">
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
														&times;
													</button>
													<h4 class="modal-title">Add new rule</h4>
												</div> <!-- end modal header -->
												<div class="modal-body">

													<div class="form-horizontal">
														<div class="form-group">
															<label for="rule_name" class="col-xs-3 col-sm-3 col-md-3 control-label">Rule Name: </label>
															<div class="col-xs-6 col-sm-6 col-md-6">
																<input type="text" class="form-control" id="rule_name" placeholder="Rule Name">
															</div>
														</div>
														<div class="form-group">
															<label for="point" class="col-xs-3 col-sm-3 col-md-3 control-label">Point(s): </label>
															<div class="col-xs-4 col-sm-4 col-md-4">
																<input type="text" class="form-control" id="point" placeholder="Point(s)" onkeyup="validateNumber(this.id, this.value)">
															</div>
														</div>
														<div class="form-group">
															<label for="range" class="col-xs-3 col-sm-3 col-md-3 control-label">Valid for: </label>
															<div class="col-xs-4 col-sm-4 col-md-4">
																<input type="text" class="form-control" id="range" placeholder="Valid for" onkeyup="validateNumber(this.id, this.value)" required/>
															</div>
															<p class="col-lg-1" style="margin-left: -3.5%; margin-top: 1%;">
																Day(s)
															</p>
														</div>
													</div>

												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-default" onclick="clearForm()" data-dismiss="modal">
														Cancel
													</button>
													<button type="button" class="btn btn-primary" onclick="addRule()" data-dismiss="modal">
														Save
													</button>
												</div>
											</div><!-- /.modal-content -->
										</div><!-- /.modal-dialog -->
									</div><!-- /.modal -->
									<div id ="editForm"></div>

								</div><!-- end panel-->
							</div><!-- end panel panel-default-->
							<div class="tab-pane" id="ruleLog">
								<div id="ruleLogTable">
								</div>
								<ul class="pager">
									<li><a style="cursor:pointer;" id="previousButton" class="navigationButton">Previous</a></li>
									<li><a style="cursor:pointer;" id="nextButton" class="navigationButton">Next</a></li>
								</ul>
							</div>
						</div>
						<form action="login.php" method="POST">
						<input type="hidden" name="signout" value="true" />
						<button type="submit" class="btn btn-danger">Sign Out</button>
					</form> <!-- end form signout -->

					</div> 
									</div><!-- end box -->
			</div> <!-- endcol-sm-12 col-md-9 col-lg-9-->
		</div>
	</div>

</div>

</div>
</div>

<div class="container box" id="footer">
	&copy; Copyright 2014 <em>Reward System</em>. All Rights Reserved.
</div>
<a href="#" id="scroll_top">Scroll</a>
<!-- Include all compiled plugins (below), or include individual files as needed -->
</body>
</html>